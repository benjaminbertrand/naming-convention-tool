#!/usr/bin/python
#
#Version: 0.01a
#
#Requirements to run this script : python and pg8000
#To install pg8000 : pip install pg8000
#
import pg8000 as pg
from classes import *
from config import *

################
####SETTINGS####
################

#Settings are under config.py
############################

ownerID = 9 # check userAccount table for correct id
sectionParents = ["","sc0" , "sb0"]
deviceTypeParents = ["ds0","" , "dv0"]
################
###CONNECTION###
################

print('Connecting do database')
connection = pg.connect(user = databaseUser, password = databasePassword, host = databaseHost, port = databasePort, database = databaseName)
cursor = connection.cursor()

################
####NAMEPART####
################
print('Creating ' + str(6) + ' nameparts')

#DeviceTypes
deviceTypeNameparts = []
for i in range(3):
    namePart = NamePart("DEVICE_TYPE")
    namePart.insert(cursor)
    deviceTypeNameparts.append(namePart.id)

#Sections
sectionNameparts = []
for i in range(3):
    namePart = NamePart("SECTION")
    namePart.insert(cursor)
    sectionNameparts.append(namePart.id)

#########################
####NAMEPARTREVISIONS####
#########################
print('Creating ' + str(6) + ' namepartrevisions')
#Generating Data
namepartrevisionValues = []
#PARENTS
namepartrevisionValues.append(NamePartRevision(0, 0, "Discipline", deviceTypeParents[0], deviceTypeNameparts[0], ownerID))
namepartrevisionValues.append(NamePartRevision(1, 0, "Device Group", deviceTypeParents[1], deviceTypeNameparts[1], ownerID, namepartrevisionValues[0]))
namepartrevisionValues.append(NamePartRevision(2, 0, "Discipline", deviceTypeParents[2], deviceTypeNameparts[2], ownerID, namepartrevisionValues[1]))

namepartrevisionValues.append(NamePartRevision(3, 0, "Super Section", sectionParents[0], sectionNameparts[0], ownerID))
namepartrevisionValues.append(NamePartRevision(4, 0, "Section", sectionParents[1], sectionNameparts[1], ownerID, namepartrevisionValues[3]))
namepartrevisionValues.append(NamePartRevision(5, 0, "Subsection", sectionParents[2], sectionNameparts[2], ownerID, namepartrevisionValues[4]))

#Inserting into database
for namePartRevision in namepartrevisionValues:
    namePartRevision.insert(cursor)

################
#####DEVICE#####
################
print('Creating ' + str(devices) + ' devices')
#Generating Data
deviceIds = []
for i in range(devices):
    device = Device()
    deviceIds.append(device.insert(cursor))


#######################
####DEVICEREVISIONS####
#######################S
print('Creating ' + str(devices) + ' devicerevisions')

#Generating Data
devicerevisionValues = []
for i in range(devices):
    devicerevisionValues.append(DeviceRevision(i , 0, getFromList(deviceIds, i) , namepartrevisionValues[2] , namepartrevisionValues[5], ownerID))
    devicerevisionValues[-1].insert(cursor)

###################
#connection.rollback()

connection.commit()


cursor.close()
connection.close()

print('Done')
