/**
 *
 */
package org.openepics.names.operation;

import java.util.List;
import java.util.Set;
import org.openepics.names.business.NameStage;
import org.openepics.names.nameViews.NameView;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import org.openepics.names.util.As;
import org.openepics.names.util.ValidationException;

/**
 * @author karinrathsman
 *
 */
public class Modify extends SingleNameOperation {

    /**
     * @param selectedNameViews the nameViews selected for the operation
     * @param root the root of the tree
     */
    public Modify(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
    }

    @Override
    protected void singleUpdate() {
        NameView nameView = singleSelectedNameView();
        NameView parentView = nameView != null ? nameView.getParent(rootType()) : getRoot();
        NameView otherParentView = nameView != null ? nameView.getParent(otherType()) : null;
        updateNameView(nameView);
        setParentView(parentView);
        updateOtherParentView(otherParentView);
    }

    @Override
    public NameStage operatedStage(NameStage stage) {
        return stage.nextRequestStage(false);
    }

    @Override
    public String getResult() {
        return isInStructure() ? "proposed to be modified " : "modified";
    }

    @Override
    protected List<NameView> affectedNameViews() {
        List<NameView> affectedNameViews = Lists.newArrayList();
        if (getNameView() != null) {
            affectedNameViews.add(getNameView());
        }
        return affectedNameViews;
    }

    @Override
    public void validateOnSelect() throws ValidationException {
        As.validateState(getNameView() != null, "Name view is null");
        for (NameView child : getNameView().getChildren()) {
            As.validateState(operationIsAllowedOnParent(getNameView(), child), "Modification is not allowed with respect to children");
        }
        As.validateState(affects(getNameView()), "Cannot modify name");
        As.validateState(acceptedAsRootParent(getParentView()) && acceptedAsOtherParent(getOtherParentView()), "Parent is not accpeted");
    }

    @Override
    public void validate() throws ValidationException {
        validateOnSelect();
        validateUpToDate();
        As.validateState(getNameElement().getFullName() != null, "Full name is null");
        As.validateState(isModify(), "No modification, the proposal equals the latest pending or approved revision");
        As.validateState(isInDeviceRegistry() || getParentView().equals(getNameView().getParent(rootType())), "Cannot move name in structure (has not yet been implemented");
    }

    @Override
    public boolean affects(NameView nameView) {
        return operatedStage(nameView.getRevisionPair().getNameStage()) != null;
    }

    @Override
    public String getTitle() {
        return isInStructure() ? "Propose to modify " : "Modify";
    }

    /**
     *
     * @return true if there are modifications, null otherwise.
     */
    public boolean isModify() {
        boolean equalsCurrent = getNameElement().equals(getNameView().getRevisionPair().getBaseRevision().getNameElement());
        boolean equalsPending = getNameView().getRevisionPair().isPending() && getNameElement().equals(getNameView().getRevisionPair().getLatestRevision().getNameElement());
        boolean sameParent = getNameView().getParent(rootType()).equals(getParentView());
        boolean sameOtherParent = Objects.equal(getNameView().getParent(otherType()), getOtherParentView());
        boolean same = (equalsCurrent || equalsPending) && sameParent && sameOtherParent;
        return !same;
    }
}
