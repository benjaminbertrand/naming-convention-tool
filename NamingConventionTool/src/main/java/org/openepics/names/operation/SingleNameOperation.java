/**
 *
 */
package org.openepics.names.operation;

import java.util.Set;

import org.openepics.names.business.NameArtifact;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameStage;
import org.openepics.names.business.NameType;
import org.openepics.names.nameViews.NameView;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * Abstract class handling data needed for single name operations (add and modify)
 *
 * @author karinrathsman
 */
public abstract class SingleNameOperation extends NameOperation {

    private NameElement nameElement;
    private NameView parentView;
    private NameView otherParentView;
    private NameView nameView;

    /**
     * @param selectedNameViews the nameViews selected for the operation
     * @param root the root of the tree
     */
    public SingleNameOperation(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
        singleUpdate();
    }

    /**
     * update after a new selection.
     */
    protected abstract void singleUpdate();

    /**
     *
     * @return true if the operation applies on the area or device structure
     */
    public boolean isInStructure() {
        return getParentView().isChildInStructure() && getOtherParentView() == null;
    }

    /**
     *
     * @return true if the operation applies on the device registry
     */
    public boolean isInDeviceRegistry() {
        return getParentView().isChildInDeviceRegistry() && getOtherParentView().isChildInDeviceRegistry();
    }

    /**
     * @return the selected nameView, if one and only one is selected. null otherwise
     */
    protected NameView singleSelectedNameView() {
        if (getSelectedNameViews().size() == 1) {
            NameView nameView = (Lists.newArrayList(getSelectedNameViews())).get(0);
            Preconditions.checkArgument(nameView == null || !nameView.getNameType().equals(otherType()));
            return nameView;
        } else {
            return null;
        }
    }

    @Override
    public boolean isMessageRequired() {
        return false;
    }

    @Override
    public Set<NameView> getAffectedDevices() {
        Set<NameView> devices = Sets.newHashSet();
        if (affectsDevices()) {
            for (NameView child : getNameView().getDevices()) {
                if (!child.getRevisionPair().getNameStage().isArchived()) {
                    devices.add(child);
                }
            }
        }
        return devices;
    }

    /**
     *
     * @return true if validation is needed, i.e. if the mnemonic or any of the parents are to be changed.
     */
    public boolean affectsDevices() {
        if (getNameView() == null || getNameView().isRoot()) {
            return false;
        } else {
            NameArtifact areaParentArtifact = parentArtifact(NameType.AREA_STRUCTURE);
            NameArtifact deviceParentArtifact = parentArtifact(NameType.DEVICE_STRUCTURE);
            return getNameView().getRevisionPair().getBaseRevision().isValidationNeededForProposedChanges(false, getNameElement(), areaParentArtifact, deviceParentArtifact);
        }
    }

    /**
     *
     * @param type the parent type
     * @return the parent artifact
     */
    private NameArtifact parentArtifact(NameType type) {
        NameView parent = getParentView(type);
        return parent == null || parent.isRoot() ? null : parent.getRevisionPair().getBaseRevision().getNameArtifact();
    }

    /**
     *
     * @param parentType the parent type
     * @return the parent of type parent Type
     */
    public NameView getParentView(NameType parentType) {
        if (rootType().equals(parentType)) {
            return getParentView();
        } else if (otherType().equals(parentType)) {
            return getOtherParentView();
        } else {
            throw new IllegalStateException("parent NameType " + parentType.toString() + " cannot be parent type");
        }
    }

    /**
     *
     * @return the parent view.
     */
    public NameView getParentView() {
        return parentView;
    }

    /**
     * sets the proposed parentView, if null the root is set as parent
     *
     * @param parentView the parentView to update
     */
    public void setParentView(NameView parentView) {
        this.parentView = parentView != null ? parentView : getRoot();
        update();
    }

    /**
     * return the proposed name Element
     *
     * @return the nameElement
     */
    public NameElement getNameElement() {
        return nameElement;
    }

    /**
     * sets the name Element
     *
     * @param nameElement the name element to set
     */
    protected void setNameElement(NameElement nameElement) {
        this.nameElement = nameElement;
    }

    /**
     * @return the otherParentView
     */
    public NameView getOtherParentView() {
        return otherParentView;
    }

    /**
     * @param otherParentView the otherParentView to set
     */
    public void updateOtherParentView(NameView otherParentView) {
        this.otherParentView = otherParentView;
        update();
    }

    /**
     * @return the nameView
     */
    public NameView getNameView() {
        return nameView;
    }

    /**
     * @param nameView the nameView to set
     */
    public void updateNameView(NameView nameView) {
        this.nameView = nameView;
        if (nameView != null) {
            setNameElement(new NameElement(nameView.getRevisionPair().getBaseRevision().getNameElement()));
        } else {
            setNameElement(new NameElement(null, null, null));
        }
        update();
    }

    /**
     *
     * @param parentView the parentView to be tested
     * @return true if the specified parent view is accepted as a parent to nameView
     */
    public boolean acceptedAsParent(NameView parentView) {
        if (!operationIsAllowedOnChild(parentView, getNameView())) {
            return false;
        }

        if (!(getNameView() == null || parentView.getLevel() == getNameView().getLevel() - 1)) {
            return false;
        }

        if (!(parentView.isChildInStructure() || parentView.isChildInDeviceRegistry() && parentView.getRevisionPair().getNameStage().isActive())) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param parentView the parentView to be tested
     * @return true if the specified parentView is accepted as parent in the root branch
     */
    public boolean acceptedAsRootParent(NameView parentView) {
        return parentView != null && parentView.getNameType().equals(rootType()) && acceptedAsParent(parentView);
    }

    /**
     *
     * @param otherParentView the parentView to be tested
     * @return true if the specified parentView is accepted as parent in the other branch
     */
    public boolean acceptedAsOtherParent(NameView otherParentView) {
        if (getParentView().isChildInStructure()) {
            return otherParentView == null;
        } else if (getParentView().isChildInDeviceRegistry()) {
            return otherParentView != null && otherParentView.isChildInDeviceRegistry() && otherParentView.getNameType().equals(otherType()) && acceptedAsParent(otherParentView);
        } else {
            return false;
        }
    }

    /**
     *
     * @param parentView the parent view
     * @param childView the child view
     * @return true if the operation is allowed on the child
     */
    protected boolean operationIsAllowedOnChild(NameView parentView, NameView childView) {
        NameStage parentStage = nameStage(parentView);
        NameStage childStage = operatedStage(nameStage(childView));
        return parentStage != null && childStage != null && parentStage.isAllowedAsParentOf(childStage);
    }

    /**
     *
     * @param parentView the parent view
     * @param childView the child view
     * @return true if the operation is allowed on the parent
     */
    protected boolean operationIsAllowedOnParent(NameView parentView, NameView childView) {
        NameStage parentStage = operatedStage(nameStage(parentView));
        NameStage childStage = nameStage(childView);
        return parentStage != null && childStage != null && parentStage.isAllowedAsParentOf(childStage);
    }
}
