/**
 *
 */
package org.openepics.names.operation;

import java.util.List;
import java.util.Set;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameStage;
import org.openepics.names.business.RowData;
import org.openepics.names.nameViews.NameView;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.openepics.names.util.As;
import org.openepics.names.util.ValidationException;

/**
 * @author karinrathsman
 *
 */
public abstract class MultipleNameOperation extends NameOperation {

    /**
     * @param selectedNameViews the nameView seletion to operate on
     * @param root the root of the name hierarchy.
     */
    public MultipleNameOperation(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
        update();
    }

    /**
     *
     * @param nameView nameView to be tested
     * @return true if the operation affects devices
     */
    protected abstract boolean affectsDevices(NameView nameView);

    @Override
    public Set<NameView> getAffectedDevices() {
        Set<NameView> devices = Sets.newHashSet();
        for (NameView nameView : getAffectedNameViews()) {
            if (affectsDevices(nameView)) {
                for (NameView child : nameView.getDevices()) {
                    if (!child.getRevisionPair().getNameStage().isArchived()) {
                        devices.add(child);
                    }
                }
            }
        }
        return devices;
    }

    /**
     *
     * @return list of devices that must be deleted before the operation can be made.
     */
    private Set<NameView> getDevicesToBeDeletedBeforeOperation() {
        // TODO fix this;
        Set<NameView> devicesToBeDeletedBeforeOperation = Sets.newHashSet();
        for (NameView nameView : getAffectedNameViews()) {
            if (nameView.isChildInDeviceRegistry() && affectsDevices(nameView)) {
                for (NameView child : nameView.getChildren()) {
                    if (!allowedAsParentChild(nameView, child)) {
                        devicesToBeDeletedBeforeOperation.add(child);
                    }
                }
            }
        }
        return devicesToBeDeletedBeforeOperation;
    }

    @Override
    protected List<NameView> affectedNameViews() {
        Set<NameView> included = Sets.newHashSet();
        for (NameView selectedNameView : getSelectedNameViews()) {
            if (!included.contains(selectedNameView) && affects(selectedNameView)) {
                included.addAll(includedWithChildren(selectedNameView));
            }
        }
        return Lists.newArrayList(included);
    }

    /**
     *
     * @param nameView to be operated on
     * @return set of nameView and children thereof that shall be operated on.
     */
    private Set<NameView> includedWithChildren(NameView includedNameView) {
        Set<NameView> included = Sets.newHashSet(includedNameView);
        NameStage nameStage = operatedStage(nameStage(includedNameView));
        for (NameView childView : includedNameView.getChildren()) {
            if (affects(childView) && !nameStage.isAllowedAsParentOf(nameStage(childView)) && nameStage.isAllowedAsParentOf(operatedStage(nameStage(childView)))) {
                included.addAll(includedWithChildren(childView));
            }
        }
        return included;
    }

    @Override
    public void validateOnSelect() throws ValidationException {
        As.validateState(!getAffectedNameViews().isEmpty(), "No names affected by the operation");
        for (NameView nameView : getAffectedNameViews()) {
            NameView parentView = nameView.getParent(rootType());
            As.validateState(allowedAsParentChild(parentView, nameView), "Name state is not allowed under the parent");
            for (NameView child : nameView.getChildren()) {
                As.validateState(allowedAsParentChild(nameView, child) || getDevicesToBeDeletedBeforeOperation().contains(child), "Name status is not allowed as parent to child states");
            }
        }
    }

    @Override
    public void validate() throws ValidationException {
        validateOnSelect();
        validateUpToDate();
    }

    @Override
    public boolean affects(NameView nameView) {
        NameStage operatedStage = operatedStage(nameStage(nameView));
        return operatedStage != null && !(operatedStage.isPending() && nameView.isInDeviceRegistry());

    }

    /**
     *
     * @param nameView the name view to be operated on.
     * @return the next approved revision after operation if the nameView is affected and included by the operation, null otherwise
     */
    protected abstract NameRevision nextApprovedRevision(NameView nameView);

    /**
     *
     * @param nameView the name view to be operated on.
     * @return the next unapproved revision after operation if the nameView is affected and included by the operation, null otherwise
     */
    protected abstract NameRevision nextUnapprovedRevision(NameView nameView);

    /**
     * @param nameView the nameView to create the row data for
     * @return the operated rowData after operation. Null if the nameView is not affected by the operation.
     */
    public RowData getOperatedRowData(NameView nameView) {
        if (getAffectedNameViews().contains(nameView)) {
            NameStage nextStage = operatedOrUnaffectedStage(nameView);
            NameRevision unapproved = nextUnapprovedRevision(nameView);
            NameRevision approved = nextApprovedRevision(nameView);
            NameElement unapprovedElement = unapproved != null ? unapproved.getNameElement() : null;
            NameElement approvedElement = approved != null ? approved.getNameElement() : null;
            return new RowData(unapprovedElement, approvedElement, null, nextStage.isPending(), nextStage.isDeleted(), nextStage.isApproved(), nextStage.isArchived(), nextStage.isCancelled(), false);
        }
        return null;
    }
}
