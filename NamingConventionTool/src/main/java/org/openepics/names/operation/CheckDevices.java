package org.openepics.names.operation;

import java.util.Set;

import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameStage;
import org.openepics.names.nameViews.NameView;
import org.openepics.names.util.As;
import org.openepics.names.util.ValidationException;

/**
 * Class handling all data needed to check that device names under this names are correctly named.
 *
 * @author karinrathsman
 */
public class CheckDevices extends MultipleNameOperation {

    /**
     * @param selectedNameViews the nameViews selected for the operation
     * @param root the root of the tree
     */
    public CheckDevices(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
    }

    @Override
    public NameStage operatedStage(NameStage stage) {
        return stage;
    }

    @Override
    public boolean isMessageRequired() {
        return false;
    }

    @Override
    public String getResult() {
        return "checked";
    }

    @Override
    public String getTitle() {
        return "Check devices";
    }

    @Override
    public boolean affectsDevices(NameView nameView) {
        return true;
    }

    @Override
    protected NameRevision nextApprovedRevision(NameView nameView) {
        return nameView.getRevisionPair().getApprovedRevision();
    }

    @Override
    protected NameRevision nextUnapprovedRevision(NameView nameView) {
        return nameView.getRevisionPair().getUnapprovedRevision();
    }

    @Override
    public void validateUser(boolean editor, boolean superUser) throws ValidationException {
        As.validateState(superUser, "User is not authorized");
    }
}
