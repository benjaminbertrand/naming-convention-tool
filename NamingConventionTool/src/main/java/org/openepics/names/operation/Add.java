/**
 *
 */
package org.openepics.names.operation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.openepics.names.business.NameStage;
import org.openepics.names.nameViews.NameView;
import org.openepics.names.util.As;
import org.openepics.names.util.ValidationException;

/**
 * Class handling all data needed to add a name.
 *
 * @author karinrathsman
 */
public class Add extends SingleNameOperation {

    /**
     * @param selectedNameViews the nameViews selected for the operation
     * @param root the root of the tree
     */
    public Add(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);

    }

    @Override
    protected void singleUpdate() {
        NameView nameView = null;
        NameView parentView = singleSelectedNameView();
        NameView otherParentView = null;
        updateNameView(nameView);
        setParentView(parentView);
        updateOtherParentView(otherParentView);
    }

    @Override
    public void validateOnSelect() throws ValidationException {
        As.validateState(acceptedAsRootParent(getParentView()) && acceptedAsOtherParent(getOtherParentView()), "parent is not accepted");
    }

    @Override
    protected List<NameView> affectedNameViews() {
        List<NameView> affectedNameViews = new ArrayList<>();
        if (getParentView() != null) {
            affectedNameViews.add(getParentView());
        }
        return affectedNameViews;
    }

    @Override
    public boolean affects(NameView parentView) {
        return acceptedAsRootParent(parentView) || acceptedAsOtherParent(parentView);
    }

    @Override
    public NameStage operatedStage(NameStage stage) {
        return stage.nextRequestStage(false);
    }

    @Override
    public String getResult() {
        return isInStructure() ? "proposed to be added" : "added";
    }

    @Override
    public String getTitle() {
        return isInStructure() ? "Propose to add " : "Add";
    }

    @Override
    public void validate() throws ValidationException {
        validateOnSelect();
        validateUpToDate();
        As.validateState(getNameElement().getFullName() != null, "Full name is null");
    }
}
