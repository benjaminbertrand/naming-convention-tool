package org.openepics.names.operation;

import java.util.Set;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameStage;
import org.openepics.names.nameViews.NameView;

public class Delete extends MultipleNameOperation {

    /**
     * @param selectedNameViews the nameViews selected for the operation
     * @param root the root of the tree
     */
    public Delete(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
    }

    @Override
    public NameStage operatedStage(NameStage stage) {
        NameStage deletedStage = stage.nextRequestStage(true);
        NameStage cancelledStage = stage.nextProcessedStage(false);
        if (deletedStage != null) {
            return deletedStage;
        } else if (cancelledStage != null && cancelledStage.isRemoved()) {
            return cancelledStage;
        } else {
            return null;
        }
    }

    @Override
    public boolean isMessageRequired() {
        return false;
    }

    @Override
    public String getResult() {
        return "removed or proposed for deletion";
    }

    @Override
    public String getTitle() {
        return "Request to Delete";
    }

    @Override
    protected boolean affectsDevices(NameView nameView) {
        return true;
    }

    @Override
    protected NameRevision nextApprovedRevision(NameView nameView) {
        return nameView.getRevisionPair().getApprovedRevision();
    }

    @Override
    protected NameRevision nextUnapprovedRevision(NameView nameView) {
        if (operatedOrUnaffectedStage(nameView).isDeleted()) {
            return nameView.getRevisionPair().getApprovedRevision();
        } else {
            return nameView.getRevisionPair().getUnapprovedRevision();
        }
    }
}
