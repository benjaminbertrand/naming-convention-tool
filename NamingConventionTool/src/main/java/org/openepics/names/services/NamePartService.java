/*-
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.names.services;

import org.openepics.names.model.*;
import org.openepics.names.util.As;
import org.openepics.names.util.JpaHelper;
import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import javax.persistence.NoResultException;

/**
 * A service bean managing NamePart and Device entities.
 *
 * @author Marko Kolar
 * @author Karin Rahtsman
 */
@Stateless
public class NamePartService {

    @Inject
    private NamingConvention namingConvention;
    @PersistenceContext
    private EntityManager em;

    /**
     * @return The list of all revisions
     */
    public List<NamePartRevision> allNamePartRevisions() {
        return em.createQuery("SELECT r FROM NamePartRevision r", NamePartRevision.class).getResultList();
    }

    /**
     * @return The list of all revisions
     */
    public List<DeviceRevision> allDeviceRevisions() {
        return em.createQuery("SELECT r FROM DeviceRevision r", DeviceRevision.class).getResultList();
    }

    /**
     * @param namePart the name part
     * @return The list of all revisions of the given name part, including approved, pending, canceled or rejected, starting from the oldest to the latest.
     */
    public List<NamePartRevision> revisions(NamePart namePart) {
        return em.createQuery("SELECT r FROM NamePartRevision r WHERE r.namePart = :namePart ORDER BY r.id", NamePartRevision.class).setParameter("namePart", namePart).getResultList();
    }

    /**
     *
     * @param namePart the name part
     * @return The current, most recent approved revision of the given name part. Null if no approved revision exists for this name part.
     */
    private @Nullable
    NamePartRevision approvedRevision(NamePart namePart) {
        return JpaHelper.getSingleResultOrNull(em.createQuery("SELECT r FROM NamePartRevision r WHERE r.namePart = :namePart AND r.status = :status ORDER BY r.id DESC", NamePartRevision.class).setParameter("namePart", namePart).setParameter("status", NamePartRevisionStatus.APPROVED).setMaxResults(1));
    }

    /**
     *
     * @param namePart the name part
     * @return The revision of the given name part currently pending approval. Null if no revision is pending approval.
     */
    private @Nullable
    NamePartRevision pendingRevision(NamePart namePart) {
        final @Nullable
        NamePartRevision lastPendingOrApprovedRevision = JpaHelper.getSingleResultOrNull(em.createQuery("SELECT r FROM NamePartRevision r WHERE r.namePart = :namePart AND (r.status = :approved OR r.status = :pending) ORDER BY r.id DESC", NamePartRevision.class).setParameter("namePart", namePart).setParameter("approved", NamePartRevisionStatus.APPROVED).setParameter("pending", NamePartRevisionStatus.PENDING).setMaxResults(1));
        if (lastPendingOrApprovedRevision == null) {
            return null;
        } else if (lastPendingOrApprovedRevision.getStatus() == NamePartRevisionStatus.PENDING) {
            return lastPendingOrApprovedRevision;
        } else {
            return null;
        }
    }

    /**
     * @return The list of all revisions of the given device, starting from the oldest to the latest.
     * @param device the device
     */
    public List<DeviceRevision> revisions(Device device) {
        return em.createQuery("SELECT r FROM DeviceRevision r WHERE r.device = :device ORDER BY r.id", DeviceRevision.class).setParameter("device", device).getResultList();
    }

    /**
     * @return The current, most recent revision of the given device
     *
     * @param device the device
     */
    public DeviceRevision currentRevision(Device device) {
        return em.createQuery("SELECT r FROM DeviceRevision r WHERE r.device = :device ORDER BY r.id DESC", DeviceRevision.class).setParameter("device", device).getResultList().get(0);
    }

    /**
     * the em attached namePartRevision in the database
     *
     * @param namePartRevision the (unattached) namePartRevision
     * @return
     */
    private NamePartRevision emAttached(NamePartRevision namePartRevision) {
        if (!em.contains(namePartRevision)) {
            return As.notNull(em.find(NamePartRevision.class, namePartRevision.getId()));
        } else {
            return namePartRevision;
        }
    }

    /**
     * @param userName The name of the user
     * @return The UserAccount of the user with the given user name.
     */
    public UserAccount userWithName(String userName) {
        return em.createQuery("SELECT u FROM UserAccount u WHERE u.username = :userName", UserAccount.class).setParameter("userName", userName).getSingleResult();
    }

    /**
     * Creates new user with the given user name if not already exists.
     *
     * @param userName The user name
     * @param role The Role
     */
    public void createUser(String userName, Role role) {
        try {
            userWithName(userName);
        } catch (NoResultException e) {
            em.persist(new UserAccount(userName, role));
        }
    }

    /**
     *
     * @param user the (possibly detached) UserAccount entity
     * @return The EntityManager-attached entity corresponding to the given UserAccount entity.
     */
    public UserAccount emAttached(UserAccount user) {
        return As.notNull(em.find(UserAccount.class, user.getId()));
    }

}
