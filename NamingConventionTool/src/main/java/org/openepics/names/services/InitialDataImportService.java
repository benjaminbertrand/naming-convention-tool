/*-
* Copyright (c) 2014 European Spallation Source
* Copyright (c) 2014 Cosylab d.d.
*
* This file is part of Naming Service.
* Naming Service is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or any newer version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.names.services;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameType;
import org.openepics.names.model.*;
import org.openepics.names.nameViews.NameView;
import org.openepics.names.nameViews.NameViewProvider;
import org.openepics.names.operation.Add;
import org.openepics.names.operation.AddDevice;
import org.openepics.names.util.As;
import org.openepics.names.util.ExcelCell;

import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openepics.names.nameViews.TransactionBean;
import org.openepics.names.operation.NameOperation;

/**
 * A service bean used to initialize the database with data from the bundled Excel file when the application is ran for the first time.
 *
 * @author Andraz Pozar
 * @author Karin Rathsman
 */
@Stateless
public class InitialDataImportService {

    @Inject
    private NameViewProvider nameViewProvider;
    @Inject
    private NamingConvention namingConvention;
    @Inject
    private NamePartService namePartService;
    @Inject
    private TransactionBean transactionBean;

    private XSSFWorkbook workbook;
    private Map<Integer, NameView> nameViewMap = Maps.newHashMap();
    private NameView areaRoot;

    /**
     * Populates the database with initial data bundled as a resource within the application.
     */
    public void fillDatabaseWithInitialData() {
        try {
            workbook = new XSSFWorkbook(this.getClass().getResourceAsStream("NamingDatabaseImport.xlsx"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
//        fillUserAccounts();
        this.areaRoot = fillNameParts(NameType.AREA_STRUCTURE);
        fillDeviceNames();

    }

    private void fillUserAccounts() {
        namePartService.createUser("namesadmin", Role.SUPERUSER);
        namePartService.createUser("nameseditor", Role.EDITOR);

//        em.persist(new UserAccount("namesadmin", Role.SUPERUSER));
//        em.persist(new UserAccount("nameseditor", Role.EDITOR));
    }

    private NameView fillNameParts(NameType type) {
        final XSSFSheet sheet = workbook.getSheet(type.isAreaStructure() ? "AreaStructure" : "DeviceStructure");
        NameView root = new NameView(type);
        for (Row row : sheet) {
            if (row.getRowNum() >= 2) {
                final int parent = (int) ExcelCell.asNumber(row.getCell(0));
                final int id = (int) ExcelCell.asNumber(row.getCell(1));
                final String name = As.notNull(ExcelCell.asString(row.getCell(2)));
                final String mnemonic = As.notNull(ExcelCell.asString(row.getCell(3)));
                @Nullable
                final String description = ExcelCell.asString(row.getCell(4));
                NameView existingParentView = nameViewMap.get(parent);
                NameView parentView = existingParentView != null && existingParentView.getRevisionPair().getNameStage().isApproved() ? existingParentView : root;
                nameViewMap.put(id, addNamePart(parentView, root, name, mnemonic, description));
            }
        }
        return root;
    }

    private void fillDeviceNames() {
        final XSSFSheet sheet = workbook.getSheet("DeviceInstances");
        final Set<NameOperation> operations = Sets.newHashSet();
        for (Row row : sheet) {
            if (row.getRowNum() >= 1) {
                final int subsectionId = (int) ExcelCell.asNumber(row.getCell(1));
                final int deviceTypeId = (int) ExcelCell.asNumber(row.getCell(2));
                @Nullable
                final String instanceIndex = ExcelCell.asString(row.getCell(3));
                @Nullable
                final String description = ExcelCell.asString(row.getCell(4));
                NameView subsectionView = nameViewMap.get(subsectionId);
                NameView deviceTypeView = nameViewMap.get(deviceTypeId);

                final String newDeviceName = namingConvention.conventionName(subsectionView.getMnemonicPath(), deviceTypeView.getMnemonicPath(), instanceIndex);
                Set<NameView> selectedSubsection = Sets.newHashSet(subsectionView);
                final AddDevice operationAddDevice = new AddDevice(selectedSubsection, areaRoot);
                operationAddDevice.updateOtherParentView(deviceTypeView);
                operationAddDevice.getNameElement().setMnemonic(instanceIndex);
                operationAddDevice.getNameElement().setFullName(newDeviceName);
                operationAddDevice.getNameElement().setDescription(description);
                operations.add(operationAddDevice);

//                devices.add(new DeviceDefinition(subsection, deviceType, instanceIndex, description));
            }
        }
        //       namePartService.batchAddDevices(devices, null);
        transactionBean.submit(operations);
    }

    private NameView addNamePart(NameView parent, NameView root, String name, String mnemonic, @Nullable String description) {
        Add operation = new Add(Sets.newHashSet(parent), root);
        operation.getNameElement().setMnemonic(mnemonic);
        operation.getNameElement().setFullName(name);
        operation.getNameElement().setDescription(description);
        transactionBean.submit(operation);
        List<NameRevision> newRevisions = operation.getNewRevisions();
        return newRevisions.size() == 1 ? nameViewProvider.nameView(newRevisions.get(0).getNameArtifact().getUuid()) : null;
    }

}
