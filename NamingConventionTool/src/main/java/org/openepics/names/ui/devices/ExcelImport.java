/*-
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.names.ui.devices;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openepics.names.business.NameType;
import org.openepics.names.nameViews.NameView;
import org.openepics.names.nameViews.NameViewProvider;
import org.openepics.names.operation.AddDevice;
import org.openepics.names.services.NamingConvention;
import org.openepics.names.util.As;
import org.openepics.names.util.ExcelCell;
import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.faces.application.FacesMessage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.openepics.names.nameViews.DeviceRecordView;
import org.openepics.names.nameViews.TransactionBean;
import org.openepics.names.operation.ModifyDevice;
import org.openepics.names.operation.NameOperation;
import org.openepics.names.operation.SingleNameOperation;
import org.openepics.names.util.ValidationException;

/**
 * A bean for importing devices from Excel.
 */
@Stateless
public class ExcelImport {

    @Inject
    private NamingConvention namingConvention;
    @Inject
    private NameViewProvider nameViewProvider;
    @Inject
    private TransactionBean transactionBean;
    private Set<String> existingDeviceNames;
    private Map<String, NameView> subsectionMap;
    private Map<String, NameView> deviceTypeMap;
    private NameView areaRoot;
    private Set<String> newDeviceNames;
    private Set<String> keys;

    private static FacesMessage failureMessage(String detail) {
        return new FacesMessage(FacesMessage.SEVERITY_ERROR, "Import failed", detail);
    }

    /**
     * Check statement ant throws exception if it is not tru
     *
     * @param statement statement to check
     * @param detail error message
     * @throws ValidationException
     */
    private static void validate(boolean statement, String detail) throws ValidationException {
        if (!statement) {
            throw new ValidationException(failureMessage(detail));
        }
    }

    /**
     * Parses the input stream read from an Excel file, creating devices in the database. If the device already exists, it's silently ignored.
     *
     * @param input the input stream
     * @param importIsModify true if modify names on import, false if adding on import
     * @return a message with the outcome of the import operation
     */
    public FacesMessage parseDeviceImportFile(InputStream input, boolean importIsModify) {
        final Set<NameOperation> operations = Sets.newHashSet();

        init();

        try {
            final XSSFWorkbook workbook = new XSSFWorkbook(input);
            final XSSFSheet sheet = workbook.getSheetAt(0);

            for (Row row : sheet) {
                if (row.getRowNum() > 0) {
                    if (row.getLastCellNum() < 6) {
                        return failureMessage("Error occurred when reading import file. Column count does not match expected value.");
                    } else {
                        int i = 0;
                        String key = null;
                        if (importIsModify) {
                            key = ExcelCell.asString(row.getCell(i++));
                        }
                        final String superSection = As.nullIfEmpty(ExcelCell.asString(row.getCell(i++)));
                        final String section = As.nullIfEmpty(ExcelCell.asString(row.getCell(i++)));
                        final String subsection = As.nullIfEmpty(ExcelCell.asString(row.getCell(i++)));
                        final String discipline = As.nullIfEmpty(ExcelCell.asString(row.getCell(i++)));
                        final String deviceType = As.nullIfEmpty(ExcelCell.asString(row.getCell(i++)));
                        final String index = As.nullIfEmpty(ExcelCell.asString(row.getCell(i++)));
                        final String description = As.nullIfEmpty(ExcelCell.asString(row.getCell(i++)));
                        boolean emptyRow = superSection == null && section == null && subsection == null && discipline == null && deviceType == null && index == null && description == null;
                        if (!emptyRow) {
                            SingleNameOperation operation = deviceOperation(key, superSection, section, subsection, discipline, deviceType, index, description, row.getRowNum());
                            if (operation != null) {
                                operations.add(operation);
                            }
                        }

                    }
                }
            }
        } catch (IOException e) {
            return failureMessage(e.getMessage());
        } catch (ValidationException ex) {
            return ex.getFacesMessage();
        }
        return transactionBean.submit(operations);
    }

    private void init() {
        keys = new HashSet<>();
        areaRoot = nameViewProvider.nameStructure(NameType.AREA_STRUCTURE);
        subsectionMap = nameViewMap(areaRoot);
        NameView deviceStructure = nameViewProvider.nameStructure(NameType.DEVICE_STRUCTURE);
        deviceTypeMap = nameViewMap(deviceStructure);
        existingDeviceNames = new HashSet<>();
        newDeviceNames = new HashSet<>();
        for (NameView subsection : subsectionMap.values()) {
            for (NameView deviceView : subsection.getChildren()) {
                if (!deviceView.getRevisionPair().getNameStage().isObsolete()) {
                    String deviceName = deviceView.getRevisionPair().getBaseRevision().getNameElement().getFullName();
                    Preconditions.checkState(!existingDeviceNames.contains(deviceName), "The device name is duplicated...");
                    existingDeviceNames.add(deviceName);
                }
            }
        }
    }

    /**
     *
     * @param root
     * @return
     */
    private Map<String, NameView> nameViewMap(NameView root) {
        Set<NameView> parents = root.getAllowedDeviceParents();
        Map<String, NameView> namePartMap = new HashMap<>();
        for (NameView parentView : parents) {
            String conventionName = transactionBean.conventionName(parentView);
            Preconditions.checkState(!namePartMap.containsKey(conventionName), "conventionnames for device parents is not unique. Database contains corrupted data");
            namePartMap.put(conventionName, parentView);
        }
        return namePartMap;
    }

    private SingleNameOperation deviceOperation(@Nullable String key, @Nullable String superSection, String section, String subsection, String discipline, String deviceType, @Nullable String instanceIndex, @Nullable String description, int rowCounter) throws ValidationException {

        final List<String> areaPath = mnemonicPath(superSection, section, subsection);
        final String subsectionName = namingConvention.conventionName(areaPath, NameType.AREA_STRUCTURE);
        final @Nullable
        NameView subsectionView = subsectionMap.get(subsectionName);

        final List<String> devicePath = mnemonicPath(discipline, null, deviceType);
        final String deviceTypeName = namingConvention.conventionName(devicePath, NameType.DEVICE_STRUCTURE);
        final @Nullable
        NameView deviceTypeView = deviceTypeMap.get(deviceTypeName);
        String errorString = "Error occurred in row " + String.valueOf(1 + rowCounter) + ":";

        validate(subsectionView != null, errorString + " Subsection name " + subsectionName + " was not found in the database.");
        validate(deviceTypeView != null, errorString + " Device type name " + deviceTypeName + " was not found in the database.");
        String newDeviceName = namingConvention.conventionName(areaPath, devicePath, trim(instanceIndex));
        validate(!newDeviceNames.contains(newDeviceName), errorString + " Device Name " + newDeviceName + "is duplicated in the import file");
        validate(key == null || !keys.contains(key), errorString + " Id " + key + "is duplicated in the import file");
        newDeviceNames.add(newDeviceName);
        SingleNameOperation operation = null;
        if (key == null) {
            Set<NameView> selectedSubsection = Sets.newHashSet(subsectionView);
            operation = new AddDevice(selectedSubsection, areaRoot);
        } else {
            NameView device = nameViewProvider.getNameViews().get(UUID.fromString(key));
            operation = new ModifyDevice(Sets.newHashSet(device), areaRoot);
            operation.setParentView(subsectionView);
        }
        operation.updateOtherParentView(deviceTypeView);
        operation.getNameElement().setMnemonic(instanceIndex);
        operation.getNameElement().setFullName(newDeviceName);
        operation.getNameElement().setDescription(description);

        if (key != null && ((ModifyDevice) operation).isModify() || key == null && !existingDeviceNames.contains(newDeviceName)) {
            try {
                transactionBean.validate(operation);
                existingDeviceNames.add(newDeviceName);
                newDeviceNames.add(newDeviceName);
                if (key != null) {
                    keys.add(key);
                }
                return operation;
            } catch (ValidationException e) {
                String detail = errorString + " " + e.getFacesMessage().getDetail();
                throw new ValidationException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", detail));
            }
        } else {
            return null;
        }
    }

    private List<String> mnemonicPath(String level1, String level2, String level3) {
        List<String> mnemonicPath = Lists.newArrayList();
        mnemonicPath.add(trim(level1));
        mnemonicPath.add(trim(level2));
        mnemonicPath.add(trim(level3));
        return mnemonicPath;
    }

    private static String trim(String string) {
        return string != null ? string.trim() : "";
    }

    /**
     * Exports devices from the database, producing a stream which can be streamed to the user over HTTP.
     *
     * @param records the list with filtered records
     * @param importIsModify specifies wheter the import is modify
     * @return an Excel input stream containing the exported data
     */
    public InputStream templateForImport(@Nullable List<DeviceRecordView> records, boolean importIsModify) {
        final XSSFWorkbook workbook = exportWorkbook(records, importIsModify);
        final InputStream inputStream;
        try {
            final File temporaryFile = File.createTempFile("temp", "xlsx");
            FileOutputStream outputStream = new FileOutputStream(temporaryFile);
            workbook.write(outputStream);
            outputStream.close();
            inputStream = new FileInputStream(temporaryFile);
            temporaryFile.delete();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return inputStream;
    }

    private XSSFWorkbook exportWorkbook(@Nullable List<DeviceRecordView> devices, boolean importIsModify) {
        final XSSFWorkbook workbook = new XSSFWorkbook();
        final XSSFSheet sheet = workbook.createSheet("ImportTemplate.csv");
        if (importIsModify) {
            sheet.protectSheet("hemligt");
        }

        final ArrayList<String> titles = new ArrayList<>();
        if (importIsModify) {
            titles.add("Id");
        }
        titles.add("Super Section");
        titles.add("Section");
        titles.add("Subsection");
        titles.add("Discipline");
        titles.add("Device Type");
        titles.add("Instance Index");
        titles.add("Description/Comment");
        XSSFFont boldFont = workbook.createFont();
        boldFont.setBold(true);
        XSSFDataFormat defaultFormat = workbook.createDataFormat();

        XSSFCellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFont(boldFont);
        headerStyle.setDataFormat(defaultFormat.getFormat("@"));
        XSSFCellStyle defaultStyle = workbook.createCellStyle();
        defaultStyle.setDataFormat(defaultFormat.getFormat("@"));
        defaultStyle.setLocked(false);

        Row head = sheet.createRow(0);
        for (int i = 0; i < titles.size(); i++) {
            if (!importIsModify) {
                sheet.getColumnHelper().setColDefaultStyle(i, defaultStyle);
            }
            Cell cell = head.createCell(i);
            cell.setCellValue(titles.get(i));
            cell.setCellStyle(headerStyle);
        }

        if (importIsModify && devices != null) {
            int rowNr = 0;
            for (DeviceRecordView record : devices) {
                if (!record.getDevice().getRevisionPair().getNameStage().isArchived()) {
                    final ArrayList<String> cells = new ArrayList<>();
                    cells.add(record.getDevice().getRevisionPair().getBaseRevision().getNameArtifact().getUuid().toString());
                    cells.add(record.getSuperSection().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                    cells.add(record.getSection().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                    cells.add(record.getSubsection().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                    cells.add(record.getDiscipline().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                    cells.add(record.getDeviceType().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                    cells.add(record.getDevice().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                    cells.add(record.getDevice().getRevisionPair().getBaseRevision().getNameElement().getDescription());
                    final Row row = sheet.createRow(++rowNr);
                    Cell cell = row.createCell(0);
                    cell.setCellValue(cells.get(0));
                    cell.setCellStyle(headerStyle);

                    for (int i = 1; i < cells.size(); i++) {
                        cell = row.createCell(i);
                        cell.setCellValue(cells.get(i));
                        cell.setCellStyle(defaultStyle);
                    }
                }
            }
        }
        for (int i = 0; i < titles.size(); i++) {
            sheet.autoSizeColumn(i);
        }
        return workbook;
    }

}
