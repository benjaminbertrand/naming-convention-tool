/*
 * This software is Copyright by the Board of Trustees of Michigan
 * State University (c) Copyright 2012.
 *
 * You may use this software under the terms of the GNU public license
 *  (GPL). The terms of this license are described at:
 *       http://www.gnu.org/licenses/gpl.txt
 *
 * Contact Information:
 *   Facility for Rare Isotope Beam
 *   Michigan State University
 *   East Lansing, MI 48824-1321
 *   http://frib.msu.edu
 *
 *
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 */
package org.openepics.names.ui.parts;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameFilter;
import org.openepics.names.business.NameType;
import org.openepics.names.business.RowData;
import org.openepics.names.nameViews.NameView;
import org.openepics.names.nameViews.NameViewProvider;
import org.openepics.names.operation.Add;
import org.openepics.names.operation.Approve;
import org.openepics.names.operation.Cancel;
import org.openepics.names.operation.Delete;
import org.openepics.names.operation.Modify;
import org.openepics.names.operation.NameOperation;
import org.openepics.names.operation.Reject;
import org.openepics.names.operation.SingleNameOperation;
import org.openepics.names.ui.common.TreeNodeManager;
import org.openepics.names.util.As;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import org.openepics.names.nameViews.TransactionBean;
import org.openepics.names.operation.CheckDevices;
import org.openepics.names.services.SessionService;
import org.openepics.names.ui.common.LoginController;
import org.openepics.names.util.ValidationException;

/**
 * A UI controller bean for the Logical Area Structure and Device Category Structure screens.
 *
 * @author Vasu V
 * @author Karin Rathsman
 */
@ManagedBean
@ViewScoped
public class NamePartsController implements Serializable {

    private static final long serialVersionUID = 3723235418421156956L;
    @Inject
    private TreeNodeManager treeNodeManager;
    @Inject
    private NameViewProvider nameViewProvider;
    @Inject
    private TransactionBean transactionBean;
    @Inject
    private LoginController loginController;
    @Inject 
    private SessionService sessionService;

    private List<RowData> historyRevisionsAsRowData;
    private TreeNode viewRoot;
    private TreeNode[] selectedNodes;
    private NameType nameType;
    private Wizard wizard;
    private NameOperation nameOperation;

    /**
     * @return the type
     */
    public String getType() {
        return nameType != null ? nameType.toString() : NameType.AREA_STRUCTURE.toString();
    }

    /**
     * initializes the bean.
     */
    @PostConstruct
    public void init() {
        String structure = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("type");
        nameType = NameType.get(structure);
        update();
    }

    /**
     * updates all relevant data after changes.
     */
    public synchronized void update() {
        viewRoot = As.notNull(treeNodeManager.getStructureTree(null, nameType));
        setSelectedNodes(null);
        nameOperation = null;
        wizard = null;
    }

    /**
     * expands all nodes in the tree.
     */
    public synchronized void onExpandAll() {
        treeNodeManager.expandAll(viewRoot);
    }

    /**
     * collapses all nodes in the tree
     */
    public synchronized void onCollapseAll() {
        treeNodeManager.collapseAll(viewRoot);
    }

    /**
     * @return the view filter used in tables and trees.
     */
    public NameFilter getViewFilter() {
        return treeNodeManager.getNameFilter();
    }

    /**
     * @return the nameOperation
     */
    public NameOperation getNameOperation() {
        return nameOperation;
    }

    /**
     *
     * @return tru if the history can be shown for selected name
     */
    public boolean canShowHistory() {
        return selectedNodes != null && selectedNodes.length == 1;
    }

    /**
     * prepare for the history popup
     */
    public void prepareHistoryPopup() {
        NameView selectedNameView = canShowHistory() ? TreeNodeManager.nameView(selectedNodes[0]) : null;
        historyRevisionsAsRowData = nameViewProvider.getHistoryRevisionsAsRowData(selectedNameView);
    }

    /**
     * @return the list of all revisionsPair as row Data of the selected node.
     */
    public List<RowData> getHistoryRevisionsAsRowData() {
        return historyRevisionsAsRowData;
    }

    /**
     *
     * @return the root of the name View tree.
     */
    public TreeNode getViewRoot() {
        return viewRoot != null ? viewRoot : new DefaultTreeNode(null, null);
    }

    /**
     * @return the selected tree nodes
     */
    public TreeNode[] getSelectedNodes() {
        return selectedNodes;
    }

    /**
     * sets the selected Nodes
     *
     * @param selectedNodes array containing the selected nodes
     */
    public void setSelectedNodes(TreeNode[] selectedNodes) {
        this.selectedNodes = selectedNodes != null ? selectedNodes : new TreeNode[0];
    }

    /**
     * @param actionString indicating the required operation
     * @return true if the selected names can be operated on
     */
    public boolean can(String actionString) {
        try {
            nameOperation(actionString).validateOnSelect();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * @param actionString indicating the required operation
     * @return true if the selected names can be operated on
     */
    public boolean rendered(String actionString) {
        try {
            nameOperation(actionString).validateUser(sessionService.isEditor(), sessionService.isSuperUser());
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    /**
     *
     * @param actionString
     * @return new name operation of instance specified by the actionString
     */
    private NameOperation nameOperation(String actionString) {
        Set<NameView> selectedNameViews = Sets.newHashSet();
        for (TreeNode treeNode : selectedNodes) {
            selectedNameViews.add(TreeNodeManager.nameView(treeNode));
        }
        NameView rootView = TreeNodeManager.nameView(viewRoot);
        switch (actionString) {
            case "add":
                return new Add(selectedNameViews, rootView);
            case "approve":
                return new Approve(selectedNameViews, rootView);
            case "cancel":
                return new Cancel(selectedNameViews, rootView);
            case "delete":
                return new Delete(selectedNameViews, rootView);
            case "modify":
                return new Modify(selectedNameViews, rootView);
            case "reject":
                return new Reject(selectedNameViews, rootView);
            case "checkDevices":
                return new CheckDevices(selectedNameViews, rootView);
            default:
                 throw new UnsupportedOperationException();
        }
    }

    /**
     * Submits the nameOperation.
     *
     */
    public synchronized void onSubmit() {
        FacesMessage message;
        message = transactionBean.submit(nameOperation);
        update();
        loginController.showMessage(message);
    }

    /**
     *
     * @return true if the name operation can be submitted.
     */
    public boolean canSubmit() {
        if (nameOperation == null || nameOperation.isMessageRequired() && nameOperation.getMessage() == null) {
            //			showMessage(null, FacesMessage.SEVERITY_ERROR, "Validation Error", "Please enter a message");
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return the wizard
     */
    public Wizard getWizard() {
        return wizard;
    }

    /**
     * prepare the operation dialog
     *
     * @param actionString string specifying the operation to be performed.
     */
    public void prepareOperationDialog(String actionString) {
        this.nameOperation = nameOperation(actionString);
        if (nameOperation instanceof SingleNameOperation) {
            this.wizard = new Wizard((SingleNameOperation) nameOperation);
        }
    }

    /**
     * Subclass to control the wizard for single name operation
     *
     * @author karinrathsman
     *
     */
    public class Wizard {

        private SiteForSuperSection siteForSuperSection;
        private TreeNode rootTree;
        private @Nullable
        TreeNode nameViewNode;
        private @Nullable
        TreeNode parentViewNode;
        private SingleNameOperation singleNameOperation;
        private final List<String> tabs = Lists.newArrayList("namePartTab", "parentTab", "descriptionTab", "finishTab");

        /**
         * constructor initialising the wizard
         *
         * @param nameOperation the single name operation data for the operaiton
         */
        public Wizard(SingleNameOperation nameOperation) {
            Preconditions.checkState(nameOperation != null, "name operation is null");
            this.singleNameOperation = nameOperation;
            this.rootTree = treeNodeManager.getStructureTree(nameOperation, nameType);
            if (nameOperation != null) {
                setNameViewNode(treeNodeManager.nodeOf(rootTree, nameOperation != null ? nameOperation.getNameView() : null));
                setParentViewNode(treeNodeManager.nodeOf(rootTree, nameOperation != null ? nameOperation.getParentView() : null));
            }
        }

        /**
         *
         * @return the single name operation
         */
        public SingleNameOperation getSingleNameOperation() {
            return singleNameOperation;
        }

        /**
         * @return the operationRoot
         */
        public TreeNode getOperationRoot() {
            return rootTree;
        }

        /**
         * updates wizard when selecting nameViewNode
         *
         * @param event the node selection event
         */
        public void onSelectNameViewNode(NodeSelectEvent event) {
            TreeNode node = event.getTreeNode();
            setNameViewNode(node);
        }

        /**
         * updates wizard when unselecting nameViewNode
         *
         * @param event the node unselect event
         */
        public void onUnselectNameViewNode(NodeUnselectEvent event) {
            setNameViewNode(null);
        }

        /**
         * updates wizard when selecting parentViewNode
         *
         * @param event the node select event
         */
        public void onSelectParentViewNode(NodeSelectEvent event) {
            TreeNode node = event.getTreeNode();
            setParentViewNode(node);
        }

        /**
         * updates wizard when unselecting parentViewNode
         *
         * @param event the node unselect event
         */
        public void onUnselectParentViewNode(NodeUnselectEvent event) {
            setParentViewNode(rootTree);
        }

        /**
         * Sets and updates the form for the name part
         *
         * @param nameViewNode the nameViewNode to set
         */
        private void setNameViewNode(TreeNode nameViewNode) {
            TreeNodeManager.updateSelected(this.nameViewNode, nameViewNode);
            this.nameViewNode = nameViewNode;
            NameView nameView = nameViewNode != null ? TreeNodeManager.nameView(nameViewNode) : null;
            if (nameView != null && nameView.isInStructure() && singleNameOperation != null) {
                singleNameOperation.updateNameView(nameView);
                setParentViewNode(nameViewNode.getParent());
                boolean isOffsite = isSuperSection() && singleNameOperation.getNameElement().getMnemonic() != null;
                siteForSuperSection = SiteForSuperSection.get(isOffsite);
            } else {
                setSiteForSuperSection(SiteForSuperSection.ONSITE);
            }
        }

        /**
         * sets and updates the form for the parent node
         *
         * @param parentViewNode the parentViewNode to set
         */
        private void setParentViewNode(TreeNode parentViewNode) {
            TreeNodeManager.updateSelected(this.parentViewNode, parentViewNode);
            this.parentViewNode = parentViewNode;
            singleNameOperation.setParentView(TreeNodeManager.nameView(parentViewNode));
        }

        /**
         *
         * @return the name definition element
         */
        public NameElement getNameElementDefinition() {
            return nameViewProvider.nameElementDefinitionForChild(singleNameOperation.getParentView());
        }

        /**
         * @return the siteForSuperSection (onsite or offsite)
         */
        public SiteForSuperSection getSiteForSuperSection() {
            return siteForSuperSection;
        }

        /**
         * @param siteForSuperSection the siteForSuperSection to set (onsite or offsite)
         */
        public void setSiteForSuperSection(SiteForSuperSection siteForSuperSection) {
            this.siteForSuperSection = siteForSuperSection;
            if (isSuperSection()) {
                singleNameOperation.getNameElement().setMnemonic(null);
            }
        }

        /**
         *
         * @return true if the single selected name is super section
         */
        private boolean isSuperSection() {
            return singleNameOperation.getParentView().isRoot() && nameType.isAreaStructure();
        }

        /**
         *
         * @return true if the changes between onsite and onsite is allowed
         */
        public boolean isSiteChangeAllowed() {
            return isSuperSection() && singleNameOperation instanceof Add;
        }

        //		/**
        //		 * @return the formNameView
        //		 */
        //		public NameView getFormNameView() {
        //			return  singleNameOperation.getNameView() !=null ? singleNameOperation.getNameView(): singleNameOperation.getRoot();
        //		}
        //		/**
        //		 *  @return the formNameView
        //		 */
        //		private NameView getFormParentView(){
        //			return singleNameOperation.getParentView();
        //		}
        /**
         *
         * @param tab number
         * @return true if the tab is rendered
         */
        public boolean isTabRendered(int tab) {
            if (tab < 0 || tab > tabs.size() - 1) {
                return false;
            }

            if (singleNameOperation instanceof Add) {
                return tab != 0;
            } else if (singleNameOperation instanceof Modify) {
                return tab != 1;
            } else {
                return true;
                //			case MOVE: return tab!=2;
            }
        }

        /**
         *
         * @param event the flow event
         * @return next tab to be shown
         */
        public String onFlowProcess(FlowEvent event) {
            final String oldStep = event.getOldStep();
            final String newStep = event.getNewStep();
            final int next = tabs.indexOf(newStep);
            final int prev = tabs.indexOf(oldStep);

            final int nextRendered = nextRendered(prev, next);
            boolean forward = next >= prev;

            if (oldStep != null && isTabRendered(prev)) {
                if (prev == 0) {
                    if (nameViewNode == null) {
                        loginController.showMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validation Error:", " Please select from list"));
                        return oldStep;
                    }
                } else if (prev == 2 && forward) {
                    try {
                        transactionBean.validateMnemonic(singleNameOperation);
                    } catch (ValidationException e) {
                        loginController.showMessage(e.getFacesMessage());
                        return oldStep;
                    }
                }
            }

            if (isTabRendered(nextRendered)) {
                switch (nextRendered) {
                    case 0:
                        TreeNodeManager.updateSelected(parentViewNode, nameViewNode);
                        break;
                    case 1:
                        TreeNodeManager.updateSelected(nameViewNode, parentViewNode);
                        break;
                    default:
                        break;
                }
            }

            return isTabRendered(next) ? newStep : tabs.get(next + 1);
        }

        /**
         *
         * @param prev the previous tab
         * @param next the next tab
         * @return the next rendered tab.
         */
        private int nextRendered(int prev, int next) {
            //TODO
            boolean forward = next >= prev;
            if (isTabRendered(next)) {
                return next;
            } else if (forward && next + 1 < tabs.size()) {
                return nextRendered(prev, next + 1);
            } else if (!forward && next > 0) {
                return nextRendered(prev, next - 1);
            } else {
                return nextRendered(0, 0);
            }
        }

//        /**
//         *
//         * @param mnemonic the mnemonic to be tested
//         * @throws ValidatorException
//         */
//        public void validateMnemonic(String mnemonic) throws ValidatorException{
//            singleNameOperation.getNameElement().setMnemonic(mnemonic);
//            transactionBean.validateMnemonic(singleNameOperation);
//        }
        /**
         *
         * @return true if a non null menmonic is required
         */
        public boolean isMnemonicRequired() {
            return transactionBean.isMnemonicRequiredForChild(singleNameOperation.getParentView()) || getSiteForSuperSection().isOffsite();
        }

//        /**
//         *
//         * @return false if a Modify operation does not modify, true otherwise
//         */
//        public boolean isModifyModify() {
//            return !(singleNameOperation instanceof Modify) || ((Modify) singleNameOperation).isModify();
//        }
        /**
         *
         * @return title to be used in headers
         */
        public String getHeader() {
            return nameViewProvider.getHeader(singleNameOperation);
        }
    }

    /**
     * Enum used to indicate if a supersection (and subsequent section, subsections and devices ) is offsite, i.e. in test stand or at in-kind collaborators
     * premises.
     *
     * @author karinrathsman
     *
     */
    private enum SiteForSuperSection {
        ONSITE, OFFSITE;

        public static SiteForSuperSection get(boolean isOffsite) {
            return isOffsite ? OFFSITE : ONSITE;
        }

        public boolean isOffsite() {
            return equals(OFFSITE);
        }
    }
}
