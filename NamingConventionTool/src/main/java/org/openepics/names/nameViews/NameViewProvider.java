package org.openepics.names.nameViews;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.openepics.names.business.NameArtifact;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameRevisionPair;
import org.openepics.names.business.NameRevisionService;
import org.openepics.names.business.NameType;
import org.openepics.names.business.RowData;
import org.openepics.names.operation.SingleNameOperation;
import org.openepics.names.services.NamingConvention;
import org.openepics.names.ui.common.TreeNodeManager;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

@ApplicationScoped
/**
 * Been that generates the nameView tree and updates name views.
 *
 * @author karinrathsman
 *
 */
public class NameViewProvider implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6916728601880286405L;
    @Inject
    private NamingConvention namingConvention;
    @Inject
    private NameRevisionService nameRevisionService;
    private NameRevisions nameRevisions;
    private NameViews nameViews;

//   @PostConstruct
    public void init() {
        nameViews = new NameViews();
        nameRevisions = new NameRevisions();
        update(nameRevisionService.revisions());
    }

    /**
     *
     * @return the nameViews
     */
    public NameViews getNameViews() {
        return nameViews;
    }

    protected synchronized boolean update(List<NameRevision> newRevisions) {
        for (NameRevision revision : newRevisions) {
            nameViews.update(revision);
            nameRevisions.update(revision);
        }
        return true;
    }

    /**
     *
     * @param uuid the unique identifier for the name instance.
     * @return the nameView for the specified uuid
     */
    public NameView nameView(UUID uuid) {
        return nameViews.get(uuid);
    }

    /**
     *
     * @param nameType the type of the structure (Area or device structure)
     * @return the root of the name structure of type nameType
     */
    public NameView nameStructure(NameType nameType) {
        return nameViews.getRoot(nameType);
    }

    /**
     * @param nameView The name view
     * @return The name element definition of the name part type to be used in dialog header and menus. Example: "Modify mnemonic for nameElement.getFullName()"
     * where nameElement can be section, subsection, discipline etc...
     */
    public NameElement nameElementDefinitionForChild(NameView nameView) {
        Preconditions.checkArgument(nameView != null);
        final List<String> path = nameView.getMnemonicPathWithChild("");
        return namingConvention.getNameElementDefinition(path, nameView.getNameType());
    }

    /**
     * Generate all revisions in history for a device
     *
     * @param nameView the nameView
     * @return List of revisions in history.
     */
    public List<NameRevision> getHistoryRevisions(NameView nameView) {
        List<NameRevision> historyRevisions = nameRevisionService.historyRevisions(nameView.getRevisionPair().getBaseRevision().getNameArtifact());
        return historyRevisions;
    }

    /**
     * find the artifact
     *
     * @param nameView the artifact of the specified nameView
     * @return the name artifact
     */
    private static NameArtifact artifact(NameView nameView) {
        return nameView != null && !nameView.isRoot() ? nameView.getRevisionPair().getBaseRevision().getNameArtifact() : null;
    }

    /**
     * Generates the Header
     *
     * @param nameOperation the single name operation
     * @return title to be used in headers
     */
    public String getHeader(SingleNameOperation nameOperation) {
        NameView parentView = nameOperation.getParentView();
        return nameOperation.getTitle() + " " + nameElementDefinitionForChild(parentView).getFullName();
    }

    /**
     * Generate the history revisions
     *
     * @param nameView the nameView
     * @return all the revision of the name as RowData.
     */
    public List<RowData> getHistoryRevisionsAsRowData(NameView nameView) {
        List<RowData> historyRevisionsAsRowData = Lists.newArrayList();
        if (nameView != null && !nameView.getRevisionPair().getNameStage().isInitial()) {
            NameRevisionPair revisionPair = new NameRevisionPair();
            List<NameRevision> historyRevisions = nameRevisionService.historyRevisions(artifact(nameView));
            for (NameRevision revision : historyRevisions) {
                if (nameView.isInStructure() && !revision.getStatus().isPending() && revision.getUserAction().getUser() != null) {
// TODO : update nameRevisions with only one user action and remove this... 
                    boolean approved = revisionPair.getNameStage().isApproved();
                    NameElement approvedElement = approved ? revisionPair.getApprovedRevision().getNameElement() : null;
                    historyRevisionsAsRowData.add(new RowData(revision.getNameElement(), approvedElement, revision.getUserAction(), true, revision.isDeleted(), approved, false, false, false));
                }
                revisionPair.update(revision);
                historyRevisionsAsRowData.add(TreeNodeManager.rowData(revisionPair, true, true));
            }
        }
        return historyRevisionsAsRowData;
    }

    /**
     * @return the restfulNameRevisionMap that maps a given device name with the latest revision with that name.
     */
    public Map<String, NameRevision> getRestfulNameRevisionMap() {
        return nameRevisions.nameRevisionMap();
    }

//    void publishToMQ(List<NameRevision> revisions) {
//        Set<UUID> keys = new HashSet<>();
//        for (NameRevision revision : revisions) {
//            keys.add(revision.getNameArtifact().getUuid());
//        }
//
//        try {
//            // Create a ConnectionFactory
//            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://janperair:61616");
//            // Create a Connection
//            Connection connection = connectionFactory.createConnection();
//            connection.start();
//            // Create a Session
//            Session session;
//            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
//            // Create the destination (Topic or Queue)
//            Destination destination = session.createQueue("NamingQ");
//            // Create a MessageProducer from the Session to the Topic or Queue
//            MessageProducer producer = session.createProducer(destination);
//            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
//            // Create a messages
//            for (UUID key : keys) {
//                TextMessage message = session.createTextMessage(key.toString());
//                producer.send(message);
//            }
//
//            // Clean up
//            session.close();
//            connection.close();
//
//        } catch (JMSException ex) {
//            Logger.getLogger(NameViewProvider.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }

    private class NameRevisions {

        private Map<String, NameRevision> nameRevisionMap;

        public NameRevisions() {
            nameRevisionMap = new HashMap<>();
        }

        @Deprecated
        public Map<String, NameRevision> nameRevisionMap() {
            return nameRevisionMap;
        }

        public NameRevision get(String conventionName) {
            return nameRevisionMap.get(conventionName);
        }

        /**
         * updates the nameRevisionMap if the specified revision supersedes the existing revision with the same name
         *
         * @param revision
         */
        protected void update(NameRevision revision) {
            if (revision.getNameArtifact().getNameType().isDeviceRegistry()) {
                String conventionName = revision.getNameElement().getFullName();
                NameRevision otherRevision = get(conventionName);
                if (revision.supersede(otherRevision)) {
                    nameRevisionMap.put(conventionName, revision);
                }
            }
        }
    }
}
