/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openepics.names.nameViews;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.openepics.names.business.NameArtifact;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameRevisionPair;
import org.openepics.names.business.NameRevisionStatus;
import org.openepics.names.business.NameType;
import org.openepics.names.model.Device;
import org.openepics.names.model.DeviceRevision;
import org.openepics.names.model.NamePart;
import org.openepics.names.model.NamePartRevision;
import org.openepics.names.model.NamePartRevisionStatus;
import org.openepics.names.operation.Add;
import org.openepics.names.operation.AddDevice;
import org.openepics.names.operation.Approve;
import org.openepics.names.operation.Cancel;
import org.openepics.names.operation.CheckDevices;
import org.openepics.names.operation.Delete;
import org.openepics.names.operation.DeleteDevices;
import org.openepics.names.operation.Modify;
import org.openepics.names.operation.ModifyDevice;
import org.openepics.names.operation.NameOperation;
import org.openepics.names.operation.Reject;
import org.openepics.names.operation.SingleNameOperation;
import org.openepics.names.services.NamingConvention;
import org.openepics.names.services.SessionService;
import org.openepics.names.util.As;
import org.openepics.names.util.ValidationException;

/**
 *
 * @author karinrathsman
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.BEAN)
public class TransactionBean {

    @Inject
    private NameViewProvider nameViewProvider;
    @Inject
    private NamingConvention namingConvention;
    @Inject
    private SessionService sessionService;
    @Resource
    private UserTransaction transaction;
    @PersistenceUnit
    EntityManagerFactory emf;
    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void init() {
    }

    /**
     * Submits the operation
     *
     * @param nameOperation instance that carries the information needed to perform the operation
     * @return the outcome message of the transaction
     */
    public synchronized FacesMessage submit(NameOperation nameOperation) {
        Set<NameOperation> operations = new HashSet<>();
        operations.add(nameOperation);
        return submit(operations);
    }

    /**
     * Submits a set of operations
     *
     * @param operations list of operations
     * @return the outcome message of the transaction *
     */
    public synchronized FacesMessage submit(Set<NameOperation> operations) {
        try {

            transaction.begin();
            em = emf.createEntityManager();
            Action action = new Action();

            for (NameOperation operation : operations) {
                if (operation instanceof Delete) {
                    action.execute((Delete) operation);
                } else if (operation instanceof Add) {
                    action.execute((Add) operation);
                } else if (operation instanceof Approve) {
                    action.execute((Approve) operation);
                } else if (operation instanceof Cancel) {
                    action.execute((Cancel) operation);
                } else if (operation instanceof Modify) {
                    action.excecute((Modify) operation);
                } else if (operation instanceof Reject) {
                    action.execute((Reject) operation);
                } else if (operation instanceof CheckDevices) {
                    action.execute((CheckDevices) operation);
                } else {
                    throw new UnsupportedOperationException("NameOperation is not supported");
                }
            }
            transaction.commit();
            boolean updated = nameViewProvider.update(action.getRevisions());
            return updated ? action.getSuccessMessage() : null;
        } catch (ValidationException ex) {
            try {
                transaction.rollback();
                return ex.getFacesMessage();
            } catch (IllegalStateException | SecurityException | SystemException ex1) {
                Logger.getLogger(TransactionBean.class.getName()).log(Level.SEVERE, null, ex1);
                return null;
            }
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) { 
            try { 
                transaction.rollback();
                return new FacesMessage(FacesMessage.SEVERITY_ERROR, "Transaction Error", ex.getMessage());
            } catch (IllegalStateException | SecurityException | SystemException ex1) {
                Logger.getLogger(TransactionBean.class.getName()).log(Level.SEVERE, null, ex1);
                return null;
            }
        }
    }

    /**
     * find the artifact and check that the revision pair is up to date
     *
     * @param nameView the artifact of the specified nameView
     * @return the name artifact
     */
    public NameArtifact artifact(NameView nameView) throws ValidationException {
        NameRevisionPair pair = nameView != null ? nameView.getRevisionPair() : null;
        NameArtifact artifact = pair != null ? pair.getBaseRevision().getNameArtifact() : null;
        if (artifact != null) {
            As.validateState(pair.getLatestRevision().isSameAs(latestRevision(artifact)), "Latest revision of name with uuid" +artifact.getUuid().toString()+ " is not up todate");
            As.validateState(pair.getBaseRevision().isSameAs(baseRevision(artifact)), "BaseRevision of name with uuid" +artifact.getUuid().toString()+ " is not up todate");
        }
        return artifact;
    }

    /**
     * Check that the proposed name is valid and unique  to naming convention.
     *
     * @param nameOperation containing the proposed name
     * @throws org.openepics.names.util.ValidationException Name validation exception
     */
    public void validateMnemonic(SingleNameOperation nameOperation) throws ValidationException {
        NameView parent = nameOperation.getParentView();
        NameView otherParent = nameOperation.getOtherParentView();
        nameOperation.validate();
        String mnemonic = As.emptyIfNull(nameOperation.getNameElement().getMnemonic());
        if (nameOperation.isInStructure()) {
            As.validateState(namingConvention.isMnemonicValid(parent.getMnemonicPathWithChild(mnemonic), nameOperation.rootType()), "Mnemonic is not valid according to naming rules");
            As.validateState(isMnemonicUniqueOrOptional(nameOperation.getNameView(), nameOperation.getParentView(), nameOperation.getNameElement().getMnemonic()), "Mnemonic is not unique according to naming rules");

        } else if (nameOperation.isInDeviceRegistry()) {
            As.validateState(namingConvention.isInstanceIndexValid(parent.getMnemonicPath(), otherParent.getMnemonicPath(), mnemonic), "Instance index is not valid according to naming rules");
            As.validateState(isInstanceIndexUnique(nameOperation), "Mnemonic is not unique according to naming rules");
        } else {
            As.validateState(false, "Operation is neither in device registry nor in structure");
        }
    }

    /**
     * Checks if the proposed name is unique
     *
     * @param nameOperation the operation containing the proposed name.
     * @return true if the proposed changes results in a unique name.
     */
    private boolean isInstanceIndexUnique(SingleNameOperation nameOperation) throws ValidationException {
        As.validateState(nameOperation.isInDeviceRegistry(), "not in deviceRegistry");
        String conventionName = namingConvention.conventionName(nameOperation.getParentView().getMnemonicPath(), nameOperation.getOtherParentView().getMnemonicPath(), nameOperation.getNameElement().getMnemonic());
        String equivalenceClass = namingConvention.equivalenceClassRepresentative(conventionName);
        if (nameOperation instanceof Modify) {
            NameArtifact artifact = artifact(nameOperation.getNameView());
            List<DeviceRevision> namesakes = em.createQuery(
                    "SELECT r FROM DeviceRevision r WHERE r.id = (SELECT MAX(r2.id) FROM DeviceRevision r2 WHERE r2.device = r.device) AND r.deleted = false AND r.conventionNameEqClass = :conventionNameEqClass AND NOT (r.device =:device)", DeviceRevision.class)
                    .setParameter("conventionNameEqClass", equivalenceClass)
                    .setParameter("device", artifact.asDevice())
                    .getResultList();
            return namesakes.isEmpty();
        } else {
            List<DeviceRevision> namesakes = em.createQuery(
                    "SELECT r FROM DeviceRevision r WHERE r.id = (SELECT MAX(r2.id) FROM DeviceRevision r2 WHERE r2.device = r.device) AND r.deleted = false AND r.conventionNameEqClass = :conventionNameEqClass", DeviceRevision.class)
                    .setParameter("conventionNameEqClass", equivalenceClass)
                    .getResultList();
            return namesakes.isEmpty();
        }
    }

    /**
     * Validates the operation data before submission
     *
     * @param operation operation data
     * @throws org.openepics.names.util.ValidationException Name validation exception
     */
    public void validate(NameOperation operation) throws ValidationException {
        operation.validate();
        operation.validateUser(sessionService.isEditor(), sessionService.isSuperUser());
        if (operation instanceof SingleNameOperation) {
            validateMnemonic((SingleNameOperation) operation);
        }
    }

    /**
     * @param nameView The view of the name
     * @param parentView The view of the parent
     * @param mnemonic the mnemonic name of the name part to be tested for uniqueness
     * @return True if the mnemonic of a name part that is to be modify is unique according to the naming convention rules.
     */
    private boolean isMnemonicUniqueOrOptional(NameView nameView, NameView parentView, String mnemonic) throws ValidationException {
        final List<String> newMnemonicPath = parentView.getMnemonicPathWithChild(mnemonic);
        if (mnemonic == null) {
            return !isMnemonicRequiredForChild(parentView) && (nameView == null || nameView.getRevisionPair().getLatestRevision().getNameElement().getMnemonic() == null);
        }
        Set<NameView> namesakes = namesakes(mnemonic);
        if (namesakes != null && !namesakes.isEmpty()) {
            for (NameView namesake : namesakes) {
                if (nameView == null || !namesake.equals(nameView)) {
                    if (!namingConvention.canMnemonicsCoexist(newMnemonicPath, parentView.getNameType(), namesake.getMnemonicPath(), namesake.getNameType())) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Checks if the mnemonic is required
     *
     * @param nameView The parent of the name part, null if the name part is at the root of the hierarchy
     * @return True if the mnemonic of a child name can be null.
     */
    public boolean isMnemonicRequiredForChild(NameView nameView) {
        return namingConvention.isMnemonicRequired(nameView.getMnemonicPathWithChild(""), nameView.getNameType());
    }

    public boolean isMnemonicRequired(SingleNameOperation operation) {
        return namingConvention.isMnemonicRequired(operation.getParentView().getMnemonicPathWithChild(""), operation.getParentView().getNameType());
    }

    /**
     * Generate the naming convention name
     *
     * @param nameView the nameView
     * @return the convention name for the base revision of the nameView.
     */
    public String conventionName(NameView nameView) {
        if (nameView.isInDeviceRegistry()) {
            return namingConvention.conventionName(nameView.getParent(NameType.AREA_STRUCTURE).getMnemonicPath(), nameView.getParent(NameType.DEVICE_STRUCTURE).getMnemonicPath(), nameView.getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
        } else if (nameView.isInStructure()) {
            return namingConvention.conventionName(nameView.getMnemonicPath(), nameView.getNameType());
        } else {
            throw new UnsupportedOperationException("Operation is neither in device registry nor in structure");
        }
    }

    /**
     * Generates the convention name for the proposed name
     *
     * @param nameOperation the single name operation containing the name proposal
     * @return the convention name for the operation
     */
    public String conventionName(SingleNameOperation nameOperation) {
        if (nameOperation.isInDeviceRegistry()) {
            return namingConvention.conventionName(nameOperation.getParentView().getMnemonicPath(), nameOperation.getOtherParentView().getMnemonicPath(), nameOperation.getNameElement().getMnemonic());
        } else if (nameOperation.isInStructure()) {
            return namingConvention.conventionName(nameOperation.getParentView().getMnemonicPath(), nameOperation.rootType());
        } else {
            throw new UnsupportedOperationException("Operation is neither in device registry nor in structure");
        }
    }

    /**
     * Generates a list with namesakes of the proposed mnemonic.
     *
     * @param mnemonic the mnemonic
     * @return list of nameViews with similar mnemonics as the specified mnemonic
     */
    public Set<NameView> namesakes(String mnemonic) throws ValidationException {
        Set<NameView> namesakes = new HashSet<>();
        String mnemonicEquivalenceClass = namingConvention.equivalenceClassRepresentative(mnemonic);
        List<NamePartRevision> revisions = em.createQuery("SELECT r FROM NamePartRevision r WHERE (r.id = (SELECT MAX(r2.id) FROM NamePartRevision r2 WHERE r2.namePart = r.namePart AND r2.status = :approved) OR r.id = (SELECT MAX(r2.id) FROM NamePartRevision r2 WHERE r2.namePart = r.namePart AND r2.status = :pending)) AND r.deleted = FALSE AND r.mnemonicEqClass = :mnemonicEquivalenceClass", NamePartRevision.class).setParameter("approved", NamePartRevisionStatus.APPROVED).setParameter("pending", NamePartRevisionStatus.PENDING).setParameter("mnemonicEquivalenceClass", mnemonicEquivalenceClass).getResultList();
        for (NamePartRevision revision : revisions) {
            NameView nameView = nameViewProvider.getNameViews().get(revision.getNamePart().getUuid());
            boolean isLatest = nameView.getRevisionPair().getLatestRevision().getId() == revision.getId();
            boolean isBase = nameView.getRevisionPair().getBaseRevision().getId() == revision.getId();
            As.validateState(isLatest || isBase, "nameView is not up to date");
            namesakes.add(nameView);
        }
        return namesakes;
    }

    /**
     * Returns the latest revision of the artifact
     *
     * @param artifact
     * @return
     */
    private NameRevision latestRevision(NameArtifact artifact) {
        if (artifact.getNameType().isDeviceRegistry()) {
            DeviceRevision revision = em.createQuery("SELECT r FROM DeviceRevision r WHERE r.device = :device ORDER BY r.id DESC", DeviceRevision.class).setParameter("device", artifact.asDevice()).getResultList().get(0);
            return new NameRevision(revision);
        } else {
            NamePartRevision revision = em.createQuery("SELECT r FROM NamePartRevision r WHERE r.id = (SELECT MAX(r2.id) FROM NamePartRevision r2 WHERE r2.namePart = :namePart ) ", NamePartRevision.class).setParameter("namePart", artifact.asNamePart()).getSingleResult();
            return new NameRevision(revision);
        }
    }

    /**
     * Returns the base revision of the artifact
     *
     * @param artifact
     * @return
     */
    private NameRevision baseRevision(NameArtifact artifact) {
        if (artifact.getNameType().isDeviceRegistry()) {
            return latestRevision(artifact);
        } else {
            try {
                NamePartRevision approvedNamePartRevision = em.createQuery("SELECT r FROM NamePartRevision r WHERE r.id = (SELECT MAX(r2.id) FROM NamePartRevision r2 WHERE r2.namePart = :namePart AND r2.status = :status) ", NamePartRevision.class).setParameter("namePart", artifact.asNamePart()).setParameter("status", NamePartRevisionStatus.APPROVED).getSingleResult();
                return new NameRevision(approvedNamePartRevision);
            } catch (NoResultException e) {
                return latestRevision(artifact);
            }
        }
    }


    private class Action {

        private Map<String, Integer> resultMap;
        private List<NameRevision> revisions;
        private NameViews detachedNameViews;

        public Action() {
            resultMap = new HashMap<>();
            revisions = new ArrayList<>();
            detachedNameViews = new NameViews();
        }

        /**
         * Adds new or processed revision to revisions, resultMap and detachedNameViews
         *
         * @param action the action
         * @param revision new revision
         */
        public void add(String action, NameRevision revision) {
            if (revision != null) {
                revisions.add(revision);
                detachedNameViews.update(revision);
                Integer number = resultMap.containsKey(action) ? resultMap.get(action) : 0;
                resultMap.put(action, ++number);
            }
        }

        /**
         * process the successMessage
         *
         */
        private FacesMessage getSuccessMessage() {
            String successMessage = null;
            for (Map.Entry<String, Integer> entry : resultMap.entrySet()) {
                String action = entry.getKey();
                Integer n = entry.getValue();
                String string;
                if (action.equals("approved") || action.equals("cancelled") || action.equals("rejected")) {
                    string = n + (n > 1 ? " name proposals have been " : " name proposal has been ") + action;
                } else {
                    string = n + (n > 1 ? " names have been " : " name has been ") + action;
                }
                if (successMessage != null) {
                    successMessage = successMessage.concat(", " + string);
                } else {
                    successMessage = string;
                }
            }
            return new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", successMessage);
        }

        /**
         *
         * @return the new or processed revisions
         */
        private List<NameRevision> getRevisions() {
            return revisions;
        }

        /**
         * Executes the deletion
         *
         * @param operation delete data
         * @throws ValidatorException
         */
        private void execute(Delete operation) throws ValidationException {
            validate(operation);
            for (NameView originalNameView : operation.getAffectedNameViews()) {
                NameView nameView = detachedNameViews.addCloneOf(originalNameView);
                NameArtifact artifact = artifact(nameView);
                NameRevisionPair pair = nameView.getRevisionPair();
                NameRevision baseRevision = pair.getBaseRevision();
                NameElement el = baseRevision.getNameElement();
                if (nameView.isInDeviceRegistry() && operation instanceof DeleteDevices) {
                    NameArtifact areaParent = baseRevision.getParent(NameType.AREA_STRUCTURE);
                    NameArtifact deviceParent = baseRevision.getParent(NameType.DEVICE_STRUCTURE);
                    String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(el.getFullName());
                    final DeviceRevision deviceRevision = new DeviceRevision(artifact.asDevice(), new Date(), sessionService.user(), true, areaParent.asNamePart(), deviceParent.asNamePart(), el.getMnemonic(), el.getFullName(), mnemonicEqClass, el.getDescription());
                    em.persist(deviceRevision);
                    add("deleted", new NameRevision(deviceRevision));
                } else {
                    if (pair.getNameStage().isAdded()) {
                        processRevision(artifact, NameRevisionStatus.CANCELLED, operation.getMessage());
                    } else if (pair.getNameStage().isActive()) {
                        if (pair.getNameStage().isModified()) {
                            processRevision(artifact, NameRevisionStatus.CANCELLED, "proposal automatically cancelled before delete");
                        }
                        NameArtifact parent = baseRevision.getParent(artifact.getNameType());
                        NamePart parentAsNamePart = parent != null ? parent.asNamePart() : null;
                        String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(el.getMnemonic());
                        final NamePartRevision namePartRevision = new NamePartRevision(artifact.asNamePart(), new Date(), sessionService.user(), operation.getMessage(), true, parentAsNamePart, el.getFullName(), el.getMnemonic(), el.getDescription(), mnemonicEqClass);
                        em.persist(namePartRevision);
                        add("proposed to be deleted", new NameRevision(namePartRevision));
                    }
                }
            }
        }

        /**
         * Executes add name
         *
         * @param operation add data
         * @throws ValidationException
         */
        private void execute(Add operation) throws ValidationException {
            validate(operation);
            NameView areaParentView = detachedNameViews.addCloneOf(operation.getParentView(NameType.AREA_STRUCTURE));
            NameView deviceParentView = detachedNameViews.addCloneOf(operation.getParentView(NameType.DEVICE_STRUCTURE));
            NameElement el = operation.getNameElement();
            if (operation instanceof AddDevice) {
                NamePart areaParent = artifact(areaParentView).asNamePart();
                NamePart deviceParent = artifact(deviceParentView).asNamePart();
                String conventionNameEqClass = namingConvention.equivalenceClassRepresentative(el.getFullName());
                Device device = new Device(UUID.randomUUID());
                DeviceRevision newRevision = new DeviceRevision(device, new Date(), sessionService.user(), false, areaParent, deviceParent, el.getMnemonic(), el.getFullName(), conventionNameEqClass, el.getDescription());
                em.persist(device);
                em.persist(newRevision);
                NameRevision nameRevision = new NameRevision(newRevision);
                add("added", nameRevision);
            } else {
                NameView parentView = operation.getParentView();
                NamePart parent = parentView.isInStructure() ? artifact(parentView).asNamePart() : null;
                NamePart namePart = new NamePart(UUID.randomUUID(), operation.rootType().asNamePartType());
                String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(el.getMnemonic());
                NamePartRevision newRevision = new NamePartRevision(namePart, new Date(), sessionService.user(), operation.getMessage(), false, parent, el.getFullName(), el.getMnemonic(), el.getDescription(), mnemonicEqClass);
                em.persist(namePart);
                em.persist(newRevision);
                NameRevision nameRevision = new NameRevision(newRevision);
                add("proposed to be added", nameRevision);
            }
        }

        /**
         * Executes modification
         *
         * @param operation modify data
         * @throws ValidationException
         */
        private void excecute(Modify operation) throws ValidationException {
            validate(operation);
            NameView nameView = detachedNameViews.addCloneOf(operation.getNameView());
            NameArtifact artifact = artifact(nameView);
            NameElement el = operation.getNameElement();

            if (operation instanceof ModifyDevice) {
                NamePart areaParent = artifact(operation.getParentView(NameType.AREA_STRUCTURE)).asNamePart();
                NamePart deviceParent = artifact(operation.getParentView(NameType.DEVICE_STRUCTURE)).asNamePart();
                String conventionNameEqClass = namingConvention.equivalenceClassRepresentative(el.getFullName());
                final DeviceRevision newRevision = new DeviceRevision(artifact.asDevice(), new Date(), sessionService.user(), false, areaParent, deviceParent, el.getMnemonic(), el.getFullName(), conventionNameEqClass, el.getDescription());
                em.persist(newRevision);
                add("modified", new NameRevision(newRevision));
            } else {
                NameView parentView = operation.getParentView();
                NamePart parent = parentView.isInStructure() ? artifact(parentView).asNamePart() : null;
                if (operation.getNameView().getRevisionPair().getNameStage().isPending()) {
                    processRevision(artifact, NameRevisionStatus.CANCELLED, "Automatically cancelled before modify");
                }
                String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(el.getMnemonic());
                NamePartRevision namePartRevision = new NamePartRevision(artifact.asNamePart(), new Date(), sessionService.user(), operation.getMessage(), false, parent, el.getFullName(), el.getMnemonic(), el.getDescription(), mnemonicEqClass);
                em.persist(namePartRevision);
                add("proposed to be modified", new NameRevision(namePartRevision));
            }
        }

        /**
         * Executes approve of a set of proposed names and if necessary updates and deletes affected deviceNames.
         *
         * @param operation approve data
         * @throws ValidationException
         */
        private void execute(Approve operation) throws ValidationException {
            validate(operation);
            for (NameView originalNameView : operation.getAffectedNameViews()) {
                if (originalNameView.isInStructure()) {
                    NameView nameView = detachedNameViews.addCloneOf(originalNameView);
                    NameArtifact artifact = artifact(nameView);
                    processRevision(artifact, NameRevisionStatus.APPROVED, operation.getMessage());
                } else {
                    throw new UnsupportedOperationException();
                }
            }

            for (NameView originalDeviceView : operation.getAffectedDevices()) {
                NameView deviceView = detachedNameViews.addCloneOf(originalDeviceView);
                NameView areaParentView = deviceView.getParent(NameType.AREA_STRUCTURE);
                NameView deviceParentView = deviceView.getParent(NameType.DEVICE_STRUCTURE);
                NameArtifact artifact = artifact(deviceView);
                NameRevision baseRevision = deviceView.getRevisionPair().getBaseRevision();
                NameElement el = baseRevision.getNameElement();
                String conventionName = conventionName(deviceView);
                NamePart areaParent = baseRevision.getParent(NameType.AREA_STRUCTURE).asNamePart();
                NamePart deviceParent = baseRevision.getParent(NameType.DEVICE_STRUCTURE).asNamePart();
                boolean deleted = deviceParentView.getRevisionPair().getNameStage().isArchived() || areaParentView.getRevisionPair().getNameStage().isArchived();
                if (deleted) {
                    String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(el.getFullName());
                    final DeviceRevision deviceRevision = new DeviceRevision(artifact.asDevice(), new Date(), sessionService.user(), true, areaParent, deviceParent, el.getMnemonic(), el.getFullName(), mnemonicEqClass, el.getDescription());
                    em.persist(deviceRevision);
                    add("deleted", new NameRevision(deviceRevision));
                } else if (!el.getFullName().equals(conventionName)) {
                    String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(conventionName);
                    final DeviceRevision deviceRevision = new DeviceRevision(artifact.asDevice(), new Date(), sessionService.user(), false, areaParent, deviceParent, el.getMnemonic(), conventionName, mnemonicEqClass, el.getDescription());
                    em.persist(deviceRevision);
                    add("modified", new NameRevision(deviceRevision));
                } else {
                    throw new ValidationException("the device is not affected");
                }
            }
        }

        /**
         * Checks and corrects corrupted devices names
         *
         * @param operation
         * @throws ValidationException
         */
        private void execute(CheckDevices operation) throws ValidationException {
            validate(operation);
            for (NameView originalDeviceView : operation.getAffectedDevices()) {
                NameView deviceView = detachedNameViews.addCloneOf(originalDeviceView);
                NameView areaParentView = deviceView.getParent(NameType.AREA_STRUCTURE);
                NameView deviceParentView = deviceView.getParent(NameType.DEVICE_STRUCTURE);
                NameArtifact artifact = artifact(deviceView);
                NameRevision baseRevision = deviceView.getRevisionPair().getBaseRevision();
                NameElement el = baseRevision.getNameElement();
                String conventionName = conventionName(deviceView);
                NamePart areaParent = baseRevision.getParent(NameType.AREA_STRUCTURE).asNamePart();
                NamePart deviceParent = baseRevision.getParent(NameType.DEVICE_STRUCTURE).asNamePart();
                boolean parentDeleted = deviceParentView.getRevisionPair().getNameStage().isArchived() || areaParentView.getRevisionPair().getNameStage().isArchived();
                if (parentDeleted) {
                    String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(el.getFullName());
                    final DeviceRevision deviceRevision = new DeviceRevision(artifact.asDevice(), new Date(), sessionService.user(), true, areaParent, deviceParent, el.getMnemonic(), el.getFullName(), mnemonicEqClass, el.getDescription());
                    em.persist(deviceRevision);
                    add("deleted", new NameRevision(deviceRevision));
                } else if (!el.getFullName().equals(conventionName)) {
                    String mnemonicEqClass = namingConvention.equivalenceClassRepresentative(conventionName);
                    final DeviceRevision deviceRevision = new DeviceRevision(artifact.asDevice(), new Date(), sessionService.user(), false, areaParent, deviceParent, el.getMnemonic(), conventionName, mnemonicEqClass, el.getDescription());
                    em.persist(deviceRevision);
                    add("modified", new NameRevision(deviceRevision));
                }
            }

        }

        /**
         * excecutes cancel of a set of names
         *
         * @param operation cancel data
         * @throws ValidationException
         */
        private void execute(Cancel operation) throws ValidationException {
            validate(operation);
            for (NameView originalNameView : operation.getAffectedNameViews()) {
                if (originalNameView.isInStructure()) {
                    NameView nameView = detachedNameViews.addCloneOf(originalNameView);
                    NameArtifact artifact = artifact(nameView);
                    processRevision(artifact, NameRevisionStatus.CANCELLED, operation.getMessage());
                } else {
                    throw new UnsupportedOperationException();
                }
            }
        }

        /**
         * executes rejection of a set of names
         *
         * @param operation reject data
         * @throws ValidationException
         */
        private void execute(Reject operation) throws ValidationException {
            validate(operation);
            for (NameView originalNameView : operation.getAffectedNameViews()) {
                if (originalNameView.isInStructure()) {
                    NameView nameView = detachedNameViews.addCloneOf(originalNameView);
                    NameArtifact artifact = artifact(nameView);
                    processRevision(artifact, NameRevisionStatus.REJECTED, operation.getMessage());
                } else {
                    throw new UnsupportedOperationException();
                }
            }
        }

        /**
         *
         * @param artifact the artifact of the device or name part
         * @param status the new status
         * @param message the successMessage with the cause for processing this name.
         * @return the new nameRevision.
         * @throws ValidationException
         */
        private void processRevision(NameArtifact artifact, NameRevisionStatus status, String message) throws ValidationException {
            NamePartRevisionStatus namePartStatus = NamePartRevisionStatus.get(status);
            As.validateState(!namePartStatus.equals(NamePartRevisionStatus.PENDING), "Processed status is Pending");
            As.validateState(!artifact.getNameType().isDeviceRegistry(), "Name is in registry and cannot be processed");
            NamePart namePart = artifact.asNamePart();
            NamePartRevision namePartRevision = em.createQuery("SELECT r FROM NamePartRevision r WHERE r.id = (SELECT MAX(r2.id) FROM NamePartRevision r2 WHERE r2.namePart = :namePart ) ", NamePartRevision.class).setParameter("namePart", namePart).getSingleResult();
            As.validateState(namePartRevision.getStatus().equals(NamePartRevisionStatus.PENDING), "Name is not pending and cannot be processed");
            As.validateState(message == null || !message.isEmpty(), "message cannot be empty");
            em.detach(namePartRevision);
            namePartRevision.setStatus(namePartStatus);
            namePartRevision.setProcessDate(new Date());
            namePartRevision.setProcessedBy(sessionService.user());
            namePartRevision.setProcessorComment(message);
            NamePartRevision attachedRevision = em.merge(namePartRevision);
            NameRevision revision = new NameRevision(attachedRevision);
            add(revision.getStatus().toString().toLowerCase(), revision);
        }

    }
}
