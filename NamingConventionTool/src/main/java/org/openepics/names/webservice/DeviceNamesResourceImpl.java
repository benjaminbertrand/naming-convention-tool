/*-
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.names.webservice;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.openepics.names.jaxb.DeviceNameElement;
import org.openepics.names.jaxb.SpecificDeviceNameResource;
import org.openepics.names.nameViews.NameViewProvider;
import org.openepics.names.jaxb.DeviceNamesResource;
import com.google.common.collect.Lists;

/**
 * This is implementation of {@link DeviceNamesResource} interface.
 *
 * @author Andraz Pozar
 */
@Stateless
public class DeviceNamesResourceImpl implements DeviceNamesResource {

    @Inject
    private SpecificDeviceNameResource specificDeviceNameSubresource;
    @Inject
    private NameViewProvider nameViewProvider;

    @Override
    public List<DeviceNameElement> getAllDeviceNames() {
        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        for (String name : nameViewProvider.getRestfulNameRevisionMap().keySet()) {
            DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceName(name);
            deviceNames.add(deviceData);
        }
        return deviceNames;
    }

    @Override
    public SpecificDeviceNameResource getSpecificDeviceNameSubresource() {
        return specificDeviceNameSubresource;
    }
}
