/*-
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.names.webservice;

import java.util.UUID;
import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.openepics.names.business.NameRevision;
import org.openepics.names.jaxb.DeviceNameElement;
import org.openepics.names.jaxb.SpecificDeviceNameResource;
import org.openepics.names.nameViews.DeviceRecordView;
import org.openepics.names.nameViews.NameView;
import org.openepics.names.nameViews.NameViewProvider;

/**
 * This is implementation of {@link SpecificDeviceNameResource} interface.
 *
 * @author Andraz Pozar
 */
@Stateless
public class SpecificDeviceNameResourceImpl implements SpecificDeviceNameResource {

    @Inject
    private NameViewProvider nameViewProvider;

    /*
	 * (non-Javadoc)
	 * @see org.openepics.names.jaxb.SpecificDeviceNameResource#getDeviceName(java.lang.String)
     */
    @Override
    public @Nullable
    DeviceNameElement getDeviceName(String string) {
        UUID uuid;
        String name;
        boolean deleted;
        boolean active;
        NameRevision revision;
        NameView nameView;
        if (nameViewProvider.getRestfulNameRevisionMap().containsKey(string)) {
            name = string;
            revision = nameViewProvider.getRestfulNameRevisionMap().get(name);
            uuid = revision.getNameArtifact().getUuid();
            nameView = nameViewProvider.nameView(uuid);
            NameRevision currentRevision = nameView.getRevisionPair().getApprovedRevision();
            deleted = currentRevision.isDeleted();
            active = !deleted && currentRevision.equals(revision);
        } else {
            try {
                uuid = UUID.fromString(string);
                nameView = nameViewProvider.nameView(uuid);
                revision = nameView.getRevisionPair().getApprovedRevision();
                name = revision.getNameElement().getFullName();
                deleted = revision.isDeleted();
                active = !deleted;
            } catch (Exception e) {
                return null;
            }
        }
        String status = deleted ? "DELETED" : active ? "ACTIVE" : "OBSOLETE";
        final DeviceNameElement deviceData = new DeviceNameElement();
        deviceData.setUuid(uuid);
        deviceData.setName(name);
        deviceData.setStatus(status);
        if (active) {
            DeviceRecordView record = new DeviceRecordView(nameView);
            deviceData.setInstanceIndex(revision.getNameElement().getMnemonic());
            deviceData.setDescription(revision.getNameElement().getDescription());
            String superSection = mnemonic(record.getSuperSection());
            if (superSection != null) {
                deviceData.setSuperSection(superSection);
            }
            deviceData.setSection(mnemonic(record.getSection()));
            deviceData.setSubSection(mnemonic(record.getSubsection()));
            deviceData.setDiscipline(mnemonic(record.getDiscipline()));
            deviceData.setDeviceType(mnemonic(record.getDeviceType()));
        }
        return deviceData;
    }

    private static String mnemonic(NameView nameView) {
        return nameView.getRevisionPair().getBaseRevision().getNameElement().getMnemonic();
    }

}
