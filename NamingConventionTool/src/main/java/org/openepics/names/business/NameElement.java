package org.openepics.names.business;

import java.util.Objects;
import org.openepics.names.util.As;

/**
 * Class handling the name elements (full name, menonic and discipline) for a revision or a new proposal.
 *
 * @author karinrathsman
 *
 */
public class NameElement {

    private String fullName;
    private String mnemonic;
    private String description;

    /**
     *
     * @param fullName the full name
     * @param mnemonic the mnemonic (letter code) for a name
     * @param description the description for a name
     */
    public NameElement(String fullName, String mnemonic, String description) {
        setFullName(fullName);
        setMnemonic(mnemonic);
        setDescription(description);
    }

    /**
     * copy contstructor
     *
     * @param nameElement the name element to "clone"
     */
    public NameElement(NameElement nameElement) {
        setMnemonic(nameElement.getMnemonic());
        setFullName(nameElement.getFullName());
        setDescription(nameElement.getDescription());
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = As.nullIfEmpty(fullName);
    }

    /**
     * @return the mnemonic
     */
    public String getMnemonic() {
        return mnemonic;
    }

    /**
     * @param mnemonic the mnemonic to set.
     */
    public void setMnemonic(String mnemonic) {
        this.mnemonic = As.nullIfEmpty(mnemonic);
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = As.nullIfEmpty(description);
    }

    @Override
    public String toString() {
        String mnemonicString = mnemonic != null ? "(" + mnemonic + ")" : null;
        if (fullName != null) {
            return mnemonicString != null ? fullName + " " + mnemonicString : fullName;
        } else {
            return mnemonicString != null ? mnemonicString : null;
        }
    }

    @Override
    public boolean equals(Object otherObject) {
        NameElement other = otherObject != null && otherObject instanceof NameElement ? (NameElement) otherObject : null;
        return other != null
                && Objects.equals(fullName, other.getFullName())
                && Objects.equals(mnemonic, other.getMnemonic())
                && Objects.equals(description, other.getDescription());
    }

    public boolean hasEqualMnemonicAs(NameElement other) {
        return Objects.equals(getMnemonic(), other.getMnemonic());
    }

    public boolean hasEqualFullNameAs(NameElement other) {
        return Objects.equals(getFullName(), other.getFullName());
    }

    public boolean hasEqualDescriptionAs(NameElement other) {
        return Objects.equals(getMnemonic(), other.getDescription());
    }

}
