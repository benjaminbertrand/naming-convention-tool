package org.openepics.names.business;

import org.openepics.names.model.NamePartType;

/**
 * Enum specifying whether it belongs to the Area Structure, Device Structure or device Registry.
 *
 * @author Karin Rathsman
 */
public enum NameType {
    AREA_STRUCTURE("areaStructure"),
    DEVICE_STRUCTURE("deviceStructure"),
    DEVICE_REGISTRY("deviceName");

    /**
     * Constructor
     */
    private String value;

    NameType(String value) {
        this.value = value;
    }

    /*
	 * (non-Javadoc)
	 * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return value;
    }

    /**
     *
     * @param string the string
     * @return the nameType for a string
     */
    public static NameType get(String string) {
        if (string == null) {
            return AREA_STRUCTURE;
        }
        for (NameType type : values()) {
            if (string.equals(type.toString())) {
                return type;
            }
        }
        throw new IllegalArgumentException();
    }

    /**
     *
     * @return true if the type is areaStructure
     */
    public boolean isAreaStructure() {
        return equals(AREA_STRUCTURE);
    }

    /**
     *
     * @return true if the nameType is deviceStructure
     */
    public boolean isDeviceStructure() {
        return equals(DEVICE_STRUCTURE);
    }

    /**
     *
     * @return true if the nameType is deviceRegistry
     */
    public boolean isDeviceRegistry() {
        return equals(DEVICE_REGISTRY);
    }

    /**
     *
     * @return the corresponding NamePartType. (To be removed with the new database)
     */
    public NamePartType asNamePartType() {
        return isDeviceStructure() ? NamePartType.DEVICE_TYPE : isAreaStructure() ? NamePartType.SECTION : null;
    }
}
