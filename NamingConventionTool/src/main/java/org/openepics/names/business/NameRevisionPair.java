package org.openepics.names.business;

import com.google.common.base.Preconditions;

/**
 * The revision pair contains the latest approved revision as well as the latest unapproved revision, which can be the either pending approval or cancelled.
 *
 * @author karinrathsman
 *
 */
public class NameRevisionPair {

    private NameRevision approvedRevision;
    private NameRevision unapprovedRevision;
    private NameStage nameStage;

    public NameRevisionPair() {
        approvedRevision = null;
        unapprovedRevision = null;
        nameStage = NameStage.INITIAL;
    }

    public NameRevisionPair(NameRevisionPair pair) {
        approvedRevision = pair.getApprovedRevision();
        unapprovedRevision = pair.getUnapprovedRevision();
        nameStage = pair.getNameStage();
    }

    /**
     * @return the approvedNameRevision
     */
    public NameRevision getApprovedRevision() {
        return approvedRevision;
    }

    /**
     *
     * @return true if the name is approved
     */
    public boolean isApproved() {
        return approvedRevision != null;
    }

    /**
     *
     * @return true if proposal is pending approval
     */
    public boolean isPending() {
        return unapprovedRevision != null && unapprovedRevision.getStatus().isPending();
    }

    /**
     *
     * @return true if the proposal was cancelled
     */
    public boolean isCancelled() {
        return unapprovedRevision != null && unapprovedRevision.getStatus().isCancelled();
    }

    /**
     *
     * @return the name stage
     */
    public NameStage getNameStage() {
        return nameStage;
    }

    /**
     *
     * @param revision the nameRevision
     * @return true if the specified revision is not null and deleted
     */
    private static boolean isDeleted(NameRevision revision) {
        return revision != null && revision.isDeleted();
    }

    /**
     * @return the unapprovedNameRevision
     */
    public NameRevision getUnapprovedRevision() {
        return unapprovedRevision;
    }

    /**
     * @return the approved revision if null, else the latest pending or cancelled revision
     */
    public NameRevision getBaseRevision() {
        return approvedRevision != null ? approvedRevision : unapprovedRevision;
    }

    /**
     *
     * @return the latest pending or canceled revision superseding the approved revision if it exist, else the latest approved revision.
     */
    public NameRevision getLatestRevision() {
        return unapprovedRevision != null ? unapprovedRevision : approvedRevision;
    }

    /**
     * update with a new revision
     *
     * @param revision the new revision to be updated
     * @return true if updated, false otherwise;
     */
    public boolean update(NameRevision revision) {
        Preconditions.checkArgument(revision != null);
        Preconditions.checkArgument(getBaseRevision() == null || revision.sameArtifact(getBaseRevision()), "Artifact is not valid");
        final boolean approved = revision.getStatus().isApproved();

        boolean updated = false;
        if (revision.supersede(approvedRevision)) {
            if (approved) {
                this.approvedRevision = revision;
                updated = true;
            }
            if (revision.supersede(unapprovedRevision)) {
                this.unapprovedRevision = approved ? null : revision;
                updated = true;
            }
        }
        if (updated) {
            nameStage = NameStage.get(isPending(), isDeleted(unapprovedRevision), isApproved(), isApproved() && isDeleted(approvedRevision), isCancelled());
        }

        Preconditions.checkState(unapprovedRevision == null || unapprovedRevision.supersede(approvedRevision));
        return updated;
    }

    /**
     *
     * @return true if validation is needed.
     */
    public boolean isValidationNeeded() {
        return nameStage.isAdded() || isPending() && approvedRevision.isValidationNeededForProposedChanges(unapprovedRevision);
    }

}
