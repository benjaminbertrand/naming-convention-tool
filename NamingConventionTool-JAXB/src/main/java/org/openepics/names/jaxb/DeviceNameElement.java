package org.openepics.names.jaxb;

import javax.xml.bind.annotation.XmlRootElement;

import java.util.UUID;

/**
 * Data transfer object representing Devices for JSON and XML serialization.
 *
 * @author Andraz Pozar
 */
@XmlRootElement
public class DeviceNameElement {

    private UUID uuid;
    private String superSection;
    private String section;
    private String subSection;
    private String discipline;
    private String deviceType;
    private String instanceIndex;
    private String name;
    private String description;
    private String status;

    /**
     * Constructor
     *
     * @param uuid the unique uuid of the name that this element is accociated with.
     * @param name the naming convention name
     * @param status of the name Element. DELETED if the name associated with this uuid has been deleted. ACTIVE if the name element is not delted and is the
     * most recent name element for this uuid. OBSOLETE if the name element is neither deleted nor active (i.e. there exists a newer version for the same uuid..
     * )
     */
    public DeviceNameElement(UUID uuid, String name, String status) {
        this.uuid = uuid;
        this.name = name;
        this.status = status;
    }

    public DeviceNameElement() {

    }

    /**
     * @return the uuid
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the superSection
     */
    public String getSuperSection() {
        return superSection;
    }

    /**
     * @param superSection the superSection to set
     */
    public void setSuperSection(String superSection) {
        this.superSection = superSection;
    }

    /**
     * @return the section
     */
    public String getSection() {
        return section;
    }

    /**
     * @param section the section to set
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     * @return the subSection
     */
    public String getSubSection() {
        return subSection;
    }

    /**
     * @param subSection the subSection to set
     */
    public void setSubSection(String subSection) {
        this.subSection = subSection;
    }

    /**
     * @return the discipline
     */
    public String getDiscipline() {
        return discipline;
    }

    /**
     * @param discipline the discipline to set
     */
    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    /**
     * @return the deviceType
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * @param deviceType the deviceType to set
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * @return the instanceIndex
     */
    public String getInstanceIndex() {
        return instanceIndex;
    }

    /**
     * @param instanceIndex the instanceIndex to set
     */
    public void setInstanceIndex(String instanceIndex) {
        this.instanceIndex = instanceIndex;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

}
